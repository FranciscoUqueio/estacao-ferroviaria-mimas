drop table Estacao CASCADE CONSTRAINTS;
CREATE TABLE Estacao(
   id_estacao       NUMBER(4) NOT NULL PRIMARY KEY,
   nome           VARCHAR(255)  NOT NULL,
   localizacao    VARCHAR(255) 
) tablespace estacao storage(initial 1M);


drop table Rota CASCADE CONSTRAINTS;
CREATE TABLE Rota(
   id_rota      NUMBER(4) NOT NULL PRIMARY KEY,
   estacao_saida           VARCHAR(255)  NOT NULL,
   estacao_chegada      VARCHAR(255) NOT NULL, 
   preco  NUMBER(10,2)  NOT NULL CHECK (preco>0),
   nome           VARCHAR(255) NOT NULL,
   id_estacao     NUMBER(4) REFERENCES Estacao(id_estacao)
) tablespace estacao storage(initial 1M);


drop table Comboio CASCADE CONSTRAINTS;
CREATE TABLE Comboio(
   id_comboio       NUMBER(4) NOT NULL PRIMARY KEY,
   numero         NUMBER(4) NOT NULL,
   lotacao        NUMBER(4) NOT NULL,
   nr_vagoes        NUMBER(4) NOT NULL,
   em_movimento      NUMBER(1) NOT NULL CHECK (em_movimento IN(0,1)),
   id_rota     NUMBER(4) REFERENCES Rota(id_rota)
) tablespace estacao storage(initial 1M);


drop table Maquinista CASCADE CONSTRAINTS;
CREATE TABLE Maquinista(
   id_maquinista      NUMBER(4) NOT NULL PRIMARY KEY,
   nome           VARCHAR(255)  NOT NULL,
   idade         NUMBER(4) NOT NULL,
   sexo          CHAR NOT NULL CHECK (sexo IN('F','f','M','m')),
   anos_exp          NUMBER(4) NOT NULL,
   nr_viagens         NUMBER(4) NOT NULL
) tablespace estacao storage(initial 1M);


drop table Operador_de_Revisao CASCADE CONSTRAINTS;
CREATE TABLE Operador_de_Revisao(
   id_oper_revisao      NUMBER(4) NOT NULL PRIMARY KEY,
   nome           VARCHAR(255)  NOT NULL,
   idade         NUMBER(4) NOT NULL,
   sexo 		CHAR NOT NULL CHECK (sexo IN('F','f','M','m'))
) tablespace estacao storage(initial 1M);


drop table Viagem CASCADE CONSTRAINTS;
CREATE TABLE Viagem(
   id_viagem      NUMBER(4) NOT NULL PRIMARY KEY,
   hora_saida           VARCHAR(6)  NOT NULL,
   hora_chegada      VARCHAR(6) NOT NULL, 
   nr_passageiros        NUMBER(4) NOT NULL,
   data_viagem		VARCHAR(15) NOT NULL,
    id_comboio     NUMBER(4) REFERENCES Comboio (id_comboio),
    id_maquinista     NUMBER(4) REFERENCES Maquinista (id_maquinista)
) tablespace estacao storage(initial 1M);

drop table Comboio_Maquinista CASCADE CONSTRAINTS;
CREATE TABLE Comboio_Maquinista(
	id_comboio NUMBER(4) REFERENCES Comboio (id_comboio),
	id_maquinista NUMBER (4) REFERENCES Maquinista (id_maquinista)
) tablespace estacao storage(initial 1M);

COMMIT;
