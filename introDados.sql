INSERT INTO estacao(id_estacao,nome,localizacao)
   VALUES (1,'Zimpeto','Av. Martires 707-Maputo');

INSERT INTO estacao(id_estacao,nome,localizacao)
   VALUES (2,'Machava','Av. S�o Damasso-Matola');

INSERT INTO estacao(id_estacao,nome,localizacao)
   VALUES (3,'Museu','Av. 24 de Julho-Maputo');

INSERT INTO estacao(id_estacao,nome,localizacao)
   VALUES (4,'Mozal','Rua dos Desportistas-Matola');

INSERT INTO estacao(id_estacao,nome,localizacao)
   VALUES (5,'Boane','Av. Da Namaacha-Matola');


INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,id_estacao,nome)
   VALUES (50,'Zimpeto','Boane',100,'Zimpeto-Boane',1);

INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao)
   VALUES (51,'Zimpeto','Museu',200,'Zimpeto-Museu',1);

INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao)
   VALUES (52,'Machava','Zimpeto',200,'Machava-Zimpeto',2);

INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao)
   VALUES (53,'Machava','Museu',250,'Machava-Museu',2);

INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao)
   VALUES (55,'Museu','Boane',350,'Museu-Boane',3);

INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao)
   VALUES (56,'Mozal','Museu',300,'Mozal-Museu',4);

INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao)
   VALUES (57,'Mozal','Zimpeto',320,'Mozal-Zimpeto',4);

INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao)
   VALUES (58,'Boane','Machava',230,'Boane-Machava',5);

INSERT INTO rota(id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao)
   VALUES (59,'Boane','Mozal',250,'Boane-Mozal',5);


INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (1,10,300,3,0,1);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (2,20,200,2,1,1);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (3,10,100,1,0,2);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (4,20,200,3,1,2);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (5,10,300,3,0,3);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (6,10,100,1,1,4);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (7,20,200,2,1,4);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (8,30,200,2,0,4);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (9,10,100,1,1,5);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (10,20,300,2,0,5);

INSERT INTO Comboio(id_comboio,numero,lotacao,nr_vagoes,em_movimento, id_rota)
   VALUES (11,30,300,3,1,5);


INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (1,'Paulo Flores',26,'M',3,10);

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (2,'Anabela Carmen',30,'F',8,30);  

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (3,'Felix Macane',40,'M',10,55);

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (4,'Marcos Basto',38,'M',7,26);   

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (5,'Ana Brito',25,'F',6,25); 

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (6,'Belta Ben',36,'F',8,10);  

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (7,'Beatriz Fernando',33,'F',5,17);  

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (8,'Bruno Pedro',40,'m',10,55);  

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (9,'Ana Beatriz',33,'F',4,22);  

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (10,'Priscila',45,'f',10,120);  

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (11,'Valerio Paulo',46,'m',12,150);  

INSERT INTO Maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)
   VALUES (12,'Ben Armando',49,'M',10,110);  



INSERT INTO Operador_de_Revisao(id_oper_revisao, nome, idade, sexo)  
   VALUES (1, 'Marques Brito',24,'M');

INSERT INTO Operador_de_Revisao(id_oper_revisao, nome, idade, sexo)  
   VALUES (2, 'Mariana Pedro',30,'F');

INSERT INTO Operador_de_Revisao(id_oper_revisao, nome, idade, sexo)  
   VALUES (3, 'Pedro Costa',40,'m');

INSERT INTO Operador_de_Revisao(id_oper_revisao, nome, idade, sexo)  
   VALUES (4, 'Marcia Feroz',50,'f');

INSERT INTO Operador_de_Revisao(id_oper_revisao, nome, idade, sexo)  
   VALUES (5, 'Mateus Ferro',38,'M');


INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (1,'12:30','14:00',250,1,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (2,'07:30','14:00',250,1,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (3,'17:30','19:00',250,1,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (4,'12:00','13:30',180,2,'08/07/2020'); 

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (5,'09:30','11:00',80,3,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (6,'08:00','10:00',180,4,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (7,'10:30','11:30',280,5,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (8,'12:30','14:00',100,6,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (9,'17:30','19:00',290,7,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (10,'15:30','17:00',290,7,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (11,'14:30','16:00',200,8,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (12,'11:30','13:00',90,9,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (13,'12:30','14:00',300,10,'08/07/2020');

INSERT INTO Viagem(id_viagem,hora_saida,hora_chegada,nr_passageiros,id_comboio,data_viagem)
   VALUES (14,'07:00','08:30',250,11,'08/07/2020');


INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (1,1);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (1,3);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (2,3);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (2,4);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (3,2);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (3,5);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (4,4);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (5,6);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (6,5);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (7,7);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (8,8);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (9,10);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (10,9);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (11,11);

INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (11,12);
INSERT INTO Comboio_Maquinista(id_comboio, id_maquinista)
   VALUES (11,12);

commit;