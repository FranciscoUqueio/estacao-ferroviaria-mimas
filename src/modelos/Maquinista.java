package modelos;

public final class Maquinista extends Funcionario{
	private byte anos_exp;
	private int nr_viagens;
	
	public Maquinista(int id_funcionario, String nome, byte idade, char genero, byte anos_exp, int nr_viagens) {
		super(id_funcionario, nome, idade, genero);
		this.anos_exp = anos_exp;
		this.nr_viagens = nr_viagens;
	}

	public Maquinista() {
		super();
	}

	public byte getAnos_exp() {
		return anos_exp;
	}

	public void setAnos_exp(byte anos_exp) {
		this.anos_exp = anos_exp;
	}

	public int getNr_viagens() {
		return nr_viagens;
	}

	public void setNr_viagens(int nr_viagens) {
		this.nr_viagens = nr_viagens;
	}

	@Override
	public String toString() {
		return super.toString()+" \nMaquinista [anos_exp=" + anos_exp + ", nr_viagens=" + nr_viagens + "]";
	}
	
	
	
}
