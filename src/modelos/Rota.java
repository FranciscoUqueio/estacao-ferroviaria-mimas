package modelos;

public class Rota {
	private int id_rota;
	private String estacao_saida;
	private String estacao_chegada;
	private float preco;
	private String nome;
	private Estacao estacao;
	public Rota(int id_rota, String estacao_saida, String estacao_chegada, float preco, String nome, Estacao estacao) {
		super();
		this.id_rota = id_rota;
		this.estacao_saida = estacao_saida;
		this.estacao_chegada = estacao_chegada;
		this.preco = preco;
		this.nome = estacao_saida+"-"+estacao_chegada;
		this.estacao = estacao;
	}
	
	public Rota() {
		super();
	}

	public int getId_rota() {
		return id_rota;
	}
	public void setId_rota(int id_rota) {
		this.id_rota = id_rota;
	}
	public String getEstacao_saida() {
		return estacao_saida;
	}
	public void setEstacao_saida(String estacao_saida) {
		this.estacao_saida = estacao_saida;
	}
	public String getEstacao_chegada() {
		return estacao_chegada;
	}
	public void setEstacao_chegada(String estacao_chegada) {
		this.estacao_chegada = estacao_chegada;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Estacao getEstacao() {
		return estacao;
	}
	public void setEstacao(int id_Estacaos) {
		this.estacao = estacao;
	}
	@Override
	public String toString() {
		return "Rota [id_rota=" + id_rota + ", estacao_saida=" + estacao_saida + ", estacao_chegada=" + estacao_chegada
				+ ", preco=" + preco + ", nome=" + nome + ", estacao=" + estacao + "]";
	}
	
	
	
}
