package modelos;

public abstract class Funcionario {
	protected int id_funcionario;
	protected String nome;
	protected byte idade;
	protected char genero;
	public Funcionario(int id_funcionario, String nome, byte idade, char genero) {
		super();
		this.id_funcionario = id_funcionario;
		this.nome = nome;
		this.idade = idade;
		this.genero = genero;
	}
	public Funcionario() {
		// TODO Auto-generated constructor stub
	}
	public int getId_funcionario() {
		return id_funcionario;
	}
	public void setId_funcionario(int id_funcionario) {
		this.id_funcionario = id_funcionario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public byte getIdade() {
		return idade;
	}
	public void setIdade(byte idade) {
		this.idade = idade;
	}
	public char getGenero() {
		return genero;
	}
	public void setGenero(char genero) {
		this.genero = genero;
	}
	@Override
	public String toString() {
		return "Funcionario [id_funcionario=" + id_funcionario + ", nome=" + nome + ", idade=" + idade + ", genero="
				+ genero + "]";
	}
	
	
}
