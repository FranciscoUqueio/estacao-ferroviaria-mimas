package modelos;

public class Viagem {
	private int id_viagem;
	private String hora_saida;
	private String  hora_chegada;
	private int nr_passageiros;
	private String data_viagem;
	public Viagem(int id_viagem, String  hora_saida, String  hora_chegada, int nr_passageiros, String data_viagem,
			Comboio comboio, Maquinista maquinista) {
		super();
		this.id_viagem = id_viagem;
		this.hora_saida = hora_saida;
		this.hora_chegada = hora_chegada;
		this.nr_passageiros = nr_passageiros;
		this.data_viagem = data_viagem;
		this.comboio = comboio;
		this.maquinista = maquinista;
	}
	private Comboio comboio;
	private Maquinista maquinista;

	public Viagem() {
		// TODO Auto-generated constructor stub
	}
	public String  getHora_saida() {
		return hora_saida;
	}
	public void setHora_saida(String hora_saida) {
		this.hora_saida = hora_saida;
	}
	public String  getHora_chegada() {
		return hora_chegada;
	}
	public void setHora_chegada(String  hora_chegada) {
		this.hora_chegada = hora_chegada;
	}
	public int getId_viagem() {
		return id_viagem;
	}
	public void setId_viagem(int id_viagem) {
		this.id_viagem = id_viagem;
	}
	public int getNr_passageiros() {
		return nr_passageiros;
	}
	public void setNr_passageiros(int nr_passageiros) {
		this.nr_passageiros = nr_passageiros;
	}
	public Comboio getComboio() {
		return comboio;
	}
	public void setComboio(Comboio comboio) {
		this.comboio = comboio;
	}
	public Maquinista getMaquinista() {
		return maquinista;
	}
	public void setMaquinista(Maquinista maquinista) {
		this.maquinista = maquinista;
	}
	@Override
	public String toString() {
		return "Viagem [id_viagem=" + id_viagem + ", hora_saida=" + hora_saida + ", hora_chegada=" + hora_chegada
				+ ", nr_passageiros=" + nr_passageiros + ", data_viagem=" + data_viagem + ", comboio=" + comboio
				+ ", maquinista=" + maquinista + "]";
	}
	public String getData_viagem() {
		return data_viagem;
	}
	public void setData_viagem(String data_viagem) {
		this.data_viagem = data_viagem;
	}
	
	

}
