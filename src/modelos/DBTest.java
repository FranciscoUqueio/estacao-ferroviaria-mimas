package modelos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TimeZone;

import oracle.jdbc.pool.OracleDataSource;

import java.io.*;
public class DBTest {
 
    /**
     * @param args the command line arguments
     */
    static String host = "jdbc:oracle:thin:@192.168.88.3:1521:XE";
    static String username = "sys as sysdba";
    static String password = "sys"; // MUDAR ESTE PASSWORD
    
    private static Connection connect=null;
    private static PreparedStatement s=null;
    private static PreparedStatement ps = null;
    private static ResultSet rs=null;
    static String sql="";
        
        
    public static int countRows(){
        int i=0;
        try {
//        	connect=DriverManager.get
        	 OracleDataSource ds = new OracleDataSource();
             ds.setURL(host);
             connect = ds.getConnection(username, password);
//            try {
//                Class.forName("oracle.jdbc.driver.OracleDriver");
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
             sql = "SELECT COUNT(*) as cnt FROM funcionario@teste";
        	ps=connect.prepareStatement(sql);
        	rs = ps.executeQuery(sql);
        	rs.next();
        	i=rs.getInt("cnt");
        	System.out.println(i);
        	rs.close();
        	ps.close();
        	connect.close();
        
        }catch(SQLException se){
          se.printStackTrace();
        }finally{
          try{
             if(ps!=null)
                    connect.close();
          }catch(SQLException se){
          }
          try{
             if(connect!=null)
                connect.close();
          }catch(SQLException se){
             se.printStackTrace();
          }
       }
        return i;
    }
       
    public static void main(String[] args)throws IOException {
//        TimeZone timeZone = TimeZone.getTimeZone("Europe/Zurich");
//                TimeZone.setDefault(timeZone);
                int i=countRows();
        System.out.println("Abilio");
    }
    
}
 