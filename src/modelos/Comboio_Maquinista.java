package modelos;

public class Comboio_Maquinista {
	private Comboio c;
	private Maquinista mq;
	public Comboio_Maquinista(Comboio c, Maquinista mq) {
		super();
		this.c = c;
		this.mq = mq;
	}
	public Comboio getC() {
		return c;
	}
	public void setC(Comboio c) {
		this.c = c;
	}
	public Maquinista getMq() {
		return mq;
	}
	public void setMq(Maquinista mq) {
		this.mq = mq;
	}
	
}
