package modelos;

public class Estacao {
	private int id_estacao;
	private String localizacao;
	private String nome;


	public Estacao(int id_estacao, String localizacao, String nome) {
		super();
		this.id_estacao = id_estacao;
		this.localizacao = localizacao;
		this.nome = nome;
	}
	
	public Estacao() {
		super();
	}

	public int getId_estacao() {
		return id_estacao;
	}
	public void setId_estacao(int id_estacao) {
		this.id_estacao = id_estacao;
	}
	public String getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public String toString() {
		return "Estacao [id=" + id_estacao + ", localizacao=" + localizacao + ", nome=" + nome + "]";
	}
	
}
