package modelos;

public class Comboio {
	private int id_comboio;
	private short numero;
	private int lotacao;
	private int nr_vagoes;
	private int em_movimento;
	private Rota rota;
	
	public Comboio(int id_comboio, short numero, int lotacao, int nr_vagoes, int em_movimento, Rota rota) {
		super();
		this.id_comboio = id_comboio;
		this.numero = numero;
		this.lotacao = lotacao;
		this.nr_vagoes = nr_vagoes;
		this.em_movimento = em_movimento;
		this.rota = rota;
	}
	public Comboio() {}
	
	public int getId_comboio() {
		return id_comboio;
	}
	public void setId_comboio(int id_comboio) {
		this.id_comboio = id_comboio;
	}
	public short getNumero() {
		return numero;
	}
	public void setNumero(short numero) {
		this.numero = numero;
	}
	public int getLotacao() {
		return lotacao;
	}
	public void setLotacao(int lotacao) {
		this.lotacao = lotacao;
	}
	public int getNr_vagoes() {
		return nr_vagoes;
	}
	public void setNr_vagoes(int nr_vagoes) {
		this.nr_vagoes = nr_vagoes;
	}
	public int isEm_movimento() {
		return em_movimento;
	}
	public void setEm_movimento(int movimento) {
		this.em_movimento = movimento;
	}
	public Rota getRota() {
		return rota;
	}
	public void setRota(Rota rota) {
		this.rota = rota;
	}
	@Override
	public String toString() {
		return "Comboio [id_comboio=" + id_comboio + ", numero=" + numero + ", lotacao=" + lotacao + ", nr_vagoes="
				+ nr_vagoes + ", em_movimento=" + em_movimento + ", rota=" + rota + "]";
	}
	
	
}
