package conexao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import oracle.jdbc.pool.OracleDataSource;



public class BaseDados {

    private static String jdbcUrl = "jdbc:oracle:thin:@localhost:1521/xe";
    private static String username = "estacao";
    private static String password = "estacao";
    private static Connection conn;

    public BaseDados() {
        super();
    }

    public static Connection conectar() {
        try {
            OracleDataSource ds = new OracleDataSource();
            ds.setURL(jdbcUrl);
            conn = ds.getConnection(username, password);
        } catch (SQLException sql) {
            JOptionPane.showMessageDialog(null, "Erro na conexao: " + sql.getMessage());
        }
//        JOptionPane.showMessageDialog(null, "conectado");
        return conn;
    }

    public static void disconectar() {
        try {
            conn.close();
        } catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null, "Erro: " + sqle.getMessage());
        }
    }


//    public static void main(String[] args) {}

}
