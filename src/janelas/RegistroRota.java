
package janelas;

import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;

import java.awt.Dialog.ModalExclusionType;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.MaskFormatter;

import conexao.BaseDados;

import java.text.DecimalFormat;
import java.text.ParseException;


public class RegistroRota extends JFrame {

	private JPanel contentPane;
	Container cont;
    PreparedStatement ps;
    ResultSet rs;
	private JTextField txtNome;
	private JTextField txtPreco;
	private JTextField txtEndereco;
	private JList rotas;
	private String nomeRotas []= {"Zimpeto-Boane","Zimpeto-Museu","Machava-Zimpeto","Machava-Museu","Museu Boane",
			"Mozal-Museu","Mozal-Zimpeto","Boane-Machava","Boane-Mozal"};
	private float preco;
	private DecimalFormat moeda;
	private String comboChegada[]= {"Boane","Museu","Zimpeto","Boane","Machava"};
	private String comboSaida[]= {"Zimpeto","Machava","Museu","Mozal","Boane"};
	private String res;
	JComboBox comboBox,comboBox_1;
	

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistroRota frame = new RegistroRota();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public RegistroRota() {
		super("Cadastrar Rota");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 726, 329);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		moeda=new DecimalFormat("###,###,###.00 Mt");
			
		JLabel lblTitulo = new JLabel("");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setText(super.getTitle());
		lblTitulo.setBounds(0, 0, 703, 60);
		contentPane.add(lblTitulo);
		
		JPanel panDados = new JPanel();
		panDados.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Dados da Rota", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panDados.setBounds(10, 75, 347, 155);
		contentPane.add(panDados);
		panDados.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 31, 56, 16);
		panDados.add(lblNome);
		
		/*txtNome = new JTextField();
		txtNome.setEditable(false);
		txtNome.setBounds(96, 29, 239, 20);
		panDados.add(txtNome);
		txtNome.setColumns(10);*/
		
		rotas=new JList(nomeRotas);
		rotas.setBounds(96, 29, 239, 20);
		rotas.setVisibleRowCount(4);
		rotas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getContentPane().add(new JScrollPane(rotas));
		panDados.add(rotas);
		
		TratamentoEv a=new TratamentoEv();
        rotas.addListSelectionListener(a);
		
		JLabel lblPreco = new JLabel("Pre\u00E7o");
		lblPreco.setBounds(12, 70, 56, 25);
		panDados.add(lblPreco);
		
		txtPreco = new JTextField();
		txtPreco.setColumns(10);
		txtPreco.setBounds(96, 75, 239, 20);
		panDados.add(txtPreco);
	
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(102, 237, 121, 26);
		btnSalvar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	novaRota();
            }
		});
		contentPane.add(btnSalvar);
		
		JButton btnActualizar = new JButton("Verificar");
		btnActualizar.setBounds(305, 237, 121, 26);
		btnActualizar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            	//new ListaRota();
            	ListaRota listaRota = new ListaRota();
				listaRota.setVisible(true);
            }
		});
		contentPane.add(btnActualizar);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(571, 410, 121, 29);
		btnSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		contentPane.add(btnSair);
		
		JPanel panEstacoes = new JPanel();
		panEstacoes.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "Esta\u00E7\u00F5es", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panEstacoes.setBounds(369, 73, 323, 157);
		contentPane.add(panEstacoes);
		panEstacoes.setLayout(null);
		
		JLabel lblEstacaoDePartida = new JLabel("Estacao de Partida");
		lblEstacaoDePartida.setBounds(12, 23, 165, 25);
		panEstacoes.add(lblEstacaoDePartida);
		
		comboBox = new JComboBox(comboSaida);
		comboBox.setBounds(12, 53, 148, 21);
		for (byte i = 0; i < comboSaida.length; i++)
		comboBox.insertItemAt(comboSaida[i], i);
		panEstacoes.add(comboBox);
		
		JLabel lblEstacaoDeChegada = new JLabel("Estacao de Chegada");
		lblEstacaoDeChegada.setBounds(12, 90, 165, 25);
		panEstacoes.add(lblEstacaoDeChegada);
		
		comboBox_1 = new JComboBox(comboChegada);
		comboBox_1.setBounds(12, 115, 148, 21);
		for (byte i = 0; i < comboChegada.length; i++)
			comboBox_1.insertItemAt(comboChegada[i], i);
		panEstacoes.add(comboBox_1);
	}
	
	private void novaRota(){
        String sql;
        int id_esta=0;

        try{
            sql="SELECT id_estacao FROM Estacao WHERE nome= '"+comboSaida[comboBox.getSelectedIndex()]+"' ";
            ps = BaseDados.conectar().prepareStatement(sql);
            rs=ps.executeQuery();
            
            while(rs.next()) {
                id_esta=rs.getInt(1);
            }   
            
        sql = "INSERT INTO Rota (id_rota,estacao_saida,estacao_chegada,preco,nome,id_estacao) VALUES (mimas.NEXTVAL,?,?,?,?,?)";
        ps = BaseDados.conectar().prepareStatement(sql);
        ps.setString(1,comboSaida[comboBox.getSelectedIndex()]);
        ps.setString(2,comboChegada[comboBox.getSelectedIndex()]);
        ps.setFloat(3, preco);
        ps.setString(4, res);
        ps.setInt(5,id_esta);
        ps.execute();   
        JOptionPane.showMessageDialog(null, "Inseriu com sucesso");
        }catch(SQLException sqlE){
            JOptionPane.showMessageDialog(null, "Erro ao tentar inserir novo cliente: "+sqlE.getMessage());
        }
    }


	private class TratamentoEv implements ListSelectionListener
	{
		public void valueChanged(ListSelectionEvent  a)
		{
			res=nomeRotas[rotas.getSelectedIndex()];
			if(res.equalsIgnoreCase(nomeRotas[0]))
			{
				txtPreco.setEditable(true);
				preco=100;
				txtPreco.setText(""+moeda.format(preco));
			}
			else if(res.equalsIgnoreCase(nomeRotas[1]))
			{
				txtPreco.setEditable(true);
				preco=200;
				txtPreco.setText(""+moeda.format(preco));
			}
			else if(res.equalsIgnoreCase(nomeRotas[2]))
			{
				txtPreco.setEditable(true);
				preco=200;
				txtPreco.setText(""+moeda.format(preco));
			}
			else if(res.equalsIgnoreCase(nomeRotas[3]))
			{
				txtPreco.setEditable(true);
				preco=250;
				txtPreco.setText(""+moeda.format(preco));
			}
			else if(res.equalsIgnoreCase(nomeRotas[4]))
			{
				txtPreco.setEditable(true);
				preco=350;
				txtPreco.setText(""+moeda.format(preco));
			}
			else if(res.equalsIgnoreCase(nomeRotas[5]))
			{
				txtPreco.setEditable(true);
				preco=300;
				txtPreco.setText(""+moeda.format(preco));
			}
			else if(res.equalsIgnoreCase(nomeRotas[6]))
			{
				txtPreco.setEditable(true);
				preco=320;
				txtPreco.setText(""+moeda.format(preco));
			}
			
			else if(res.equalsIgnoreCase(nomeRotas[7]))
			{
				txtPreco.setEditable(true);
				preco=230;
				txtPreco.setText(""+moeda.format(preco));
			}
			
			else if(res.equalsIgnoreCase(nomeRotas[8]))
			{
				txtPreco.setEditable(true);
				preco=250;
				txtPreco.setText(""+moeda.format(preco));
			}
			
		}
	}
}
