package janelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import conexao.BaseDados;
import modelos.Validacoes;

import javax.swing.JButton;
import javax.swing.JTextField;

public class RegistroEstacao extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtLocalizacao;
	PreparedStatement ps;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistroEstacao frame = new RegistroEstacao();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistroEstacao() {
		setTitle("Cadastro de Esta\u00E7\u00E3o");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 451, 296);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitulo = new JLabel("");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setText(super.getTitle());
		lblTitulo.setBounds(0, 0, 429, 60);
		contentPane.add(lblTitulo);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(10, 207, 115, 29);
		btnSalvar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				addEstacao();
			}
		});
		contentPane.add(btnSalvar);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.setBounds(152, 207, 115, 29);
		contentPane.add(btnActualizar);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(299, 207, 115, 29);
		btnSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
			}
		});
		contentPane.add(btnSair);
		
		JPanel panDados = new JPanel();
		panDados.setBounds(10, 64, 404, 122);
		contentPane.add(panDados);
		panDados.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(15, 28, 77, 25);
		panDados.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(107, 25, 282, 31);
		panDados.add(txtNome);
		txtNome.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				Validacoes validacoes = new Validacoes();
				validacoes.validateName(txtNome.getText());
			} 
			@Override
			public void focusGained(FocusEvent arg0) {
				
			}
		});
		txtNome.setColumns(10);
		
		JLabel lblLocalizacao = new JLabel("Localizacao");
		lblLocalizacao.setBounds(15, 75, 84, 25);
		panDados.add(lblLocalizacao);
	
		txtLocalizacao = new JTextField();
		txtLocalizacao.setColumns(10);
		txtLocalizacao.setBounds(107, 72, 282, 31);
		panDados.add(txtLocalizacao);
	}
	
	public void addEstacao() {
		String plscript="DECLARE\n\tl_id NUMBER DEFAULT 0;\nBEGIN\n\tSELECT MAX(id_estacao)+1\nINTO"
				+ "\n\tl_id\nFROM\n\testacao;";
		String sql = "INSERT INTO estacao(id_estacao,nome,localizacao) VALUES (l_id,?,?);";
		plscript += sql+"\nEND;";
		System.out.println(plscript);
		executeDataBaseScript(plscript);
	}
	public void executeDataBaseScript(String pl) {
		try {
			ps=BaseDados.conectar().prepareStatement(pl);
			ps.setString(1,txtNome.getText());
			ps.setString(2,txtLocalizacao.getText());
			ps.execute();
			JOptionPane.showMessageDialog(null,"Inserido com Sucesso!");
			System.out.println("Inserido!!!");
		}catch(SQLException e) {JOptionPane.showMessageDialog(null,e);}
	}
}