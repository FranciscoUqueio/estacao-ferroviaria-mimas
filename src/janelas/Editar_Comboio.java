package janelas;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import janelas.ListaComboio;
import conexao.BaseDados;

public class Editar_Comboio extends JFrame {

	JFrame frame;
	PreparedStatement ps = null;
	ResultSet rs = null;
	private JLabel jLabel1 = new JLabel();
	private JLabel jLabel2=new JLabel();
	private JLabel lblNum=new JLabel();
	private JButton jButton1=new JButton();
	private JButton jButton2=new JButton();
	private JPanel jPanel1=new JPanel();
	private JPanel jPanel2=new JPanel();
	private int num;
	private JTextField txtNumero=new JTextField();
	private String comboNums[]= {"0","1"};
	private JComboBox comboNum;

	public Editar_Comboio(int nm) {
		super();
		num=nm;
		comboNum = new JComboBox();

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setLocationRelativeTo(null);

		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		this.setSize(new Dimension(399, 319));
		this.getContentPane().setLayout(null);
		jLabel1.setText("Editar Comboio");
		jLabel1.setBounds(new Rectangle(100, 25, 240, 40));
		jLabel1.setFont(new Font("Tahoma", 2, 35));
		lblNum.setText("Situa��o do Comboio");
		lblNum.setBounds(new Rectangle(15, 45, 150, 25));
		
		jButton1.setText("Voltar");
		jButton1.setBounds(new Rectangle(130, 235, 75, 21));
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton1_actionPerformed(e);
			}
		});
		jButton2.setText("Editar");
		jButton2.setBounds(new Rectangle(250, 235, 75, 21));
		jButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton2_actionPerformed(e);
			}
		});
		//jPanel1.setBounds(new Rectangle(215, 165, 10, 10));

		jPanel2.setBounds(new Rectangle(80, 90, 300, 115));

		jLabel2.setText("Lota��o");
		jLabel2.setBounds(new Rectangle(15, 75, 60, 25));
		txtNumero.setBounds(new Rectangle(100, 75, 60, 25));
		txtNumero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		txtNumero.addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent arg0) {
				txtNumero.addKeyListener(new KeyAdapter() {
				});
			}

			public void keyPressed(KeyEvent e) {
			}

			public void keyReleased(KeyEvent e) {
				String h = (txtNumero.getText());
				txtNumero.setText(h);

			}

		});

		jPanel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(),
				"Edite a situa��o e a lota��o do comboio", 2, 2, new Font("tahoma", Font.BOLD + Font.ITALIC, 14)));
		jPanel2.setLayout(null);

		comboNum.setBounds(new Rectangle(180, 45, 55, 20));
		for (byte i = 0; i < comboNums.length; i++)
			comboNum.insertItemAt(comboNums[i], i);
		
		jPanel2.add(lblNum, null);
		jPanel2.add(txtNumero, null);
		jPanel2.add(lblNum,null);
		jPanel2.add(comboNum, null);

		this.getContentPane().add(jPanel2, null);
		this.getContentPane().add(jButton2, null);
		this.getContentPane().add(jButton1, null);
		this.getContentPane().add(jLabel1, null);
	}

	private void update() {
		try {

			String consulta = "UPDATE comboio SET em_movimento='" + comboNums[comboNum.getSelectedIndex()]
					+ "',lotacao='"+Integer.parseInt(txtNumero.getText())+"' WHERE id_comboio = '" + num + "' ";

			ps = BaseDados.conectar().prepareStatement(consulta);
			ps.execute();
			JOptionPane.showMessageDialog(null, "Editou com  sucesso");

		} catch (SQLException sqlE) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar editar dados do comboio: " + sqlE.getMessage());
		}

	}

	private void jButton2_actionPerformed(ActionEvent e) {
		update();
	}

	private void jButton1_actionPerformed(ActionEvent e) {
		ListaComboio l = new ListaComboio();
		dispose();
	}

	
}
