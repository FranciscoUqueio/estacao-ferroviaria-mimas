package janelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Window;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.MaskFormatter;

import conexao.BaseDados;
import modelos.Comboio;
import modelos.Rota;

import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.border.MatteBorder;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout.Alignment;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.AbstractListModel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

public class RegistroFuncionario extends JFrame {
	private JPanel contentPane,panel;
	private JTextField txtNome;
	private JFormattedTextField txtdata;
	private JLabel lblNome,lblDataNascimento,lblTipoDeFuncionrio;
	private JRadioButton radMasculino,radFemenino;
	private ButtonGroup grp;
	private JComboBox cbxTipoFuncionario;
	private JLabel lblAnosDeExperiencia;
	private JSpinner spnAnos;
	private JLabel lblNumeroDeViagens;
	private JSpinner spnNrViagens;
	private JPanel panel_1;
	private JButton btnSalvar;
	private JButton btnActualizar;
	private JButton btnSair;
	private Vector<Comboio> v= new Vector<Comboio>();
	private PreparedStatement ps=null;
	private ResultSet rs;
	private JScrollPane scrollPane;
	private JList<String> list;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistroFuncionario frame = new RegistroFuncionario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ParseException 
	 */
	public RegistroFuncionario() throws ParseException {
		super("Cadastrar Funcion�rio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 649, 490);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)), "Dados Funcion\u00E1rio", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(15, 70, 605, 197);
		contentPane.add(panel);
		panel.setLayout(null);
		
		lblNome = new JLabel("Nome Completo");
		lblNome.setBounds(15, 35, 141, 20);
		panel.add(lblNome);
		
		txtNome = new JTextField(50);
		lblNome.setLabelFor(txtNome);
		txtNome.setBounds(177, 32, 272, 26);
		panel.add(txtNome);
		
		lblDataNascimento = new JLabel("Data Nascimento");
		lblDataNascimento.setBounds(15, 77, 119, 20);
		panel.add(lblDataNascimento);
		
		MaskFormatter formatador=new MaskFormatter("##/##/####");
		txtdata = new JFormattedTextField(formatador);
		txtdata.setHorizontalAlignment(SwingConstants.CENTER);
		lblDataNascimento.setLabelFor(txtdata);
		txtdata.setBounds(177, 74, 146, 26);

		txtdata.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {				
				String data=txtdata.getText();
//				System.out.println("Data: "+data);
				try {
					byte dia=Byte.parseByte(data.substring(0, 2));
//					System.out.println("Dia: "+dia);
					if(dia<0 || dia>31) {
						JOptionPane.showMessageDialog(null, "Dia Invalido");
						txtdata.setText("");						
					}
					byte mes=Byte.parseByte(data.substring(3,5));
					if(mes<1 || mes>12) {
						JOptionPane.showMessageDialog(null, "Mes Invalido");
						if(dia<10)
							txtdata.setText("0"+dia);				
						else
							txtdata.setText(""+dia);											
					}else
						switch(mes) {
						case 4: case 6: case 9:
						case 11:
							if(dia<0 || dia>30) {
								JOptionPane.showMessageDialog(null, "Dia Invalido\n Dia incopativel com o mes");
								txtdata.setText("");						
							}
							break;
						case 2: 
							if(dia<0 || dia>29) {
								JOptionPane.showMessageDialog(null, "Dia Invalido\n Dia incopativel com o mes");
								txtdata.setText("");						
							}
							break;
						}
//					System.out.println("mes: "+mes);
					int ano=Integer.parseInt(data.substring(6,10));
//					System.out.println("ano: "+ano);
					if(ano<1940) { 
						JOptionPane.showMessageDialog(null, "Idade m�xima aceite � 80 anos ");
						txtdata.select(6, 10);
						txtdata.setSelectedTextColor(Color.RED);
					}
					else
						if(( ano>Integer.parseInt(new SimpleDateFormat("yyyy").format(System.currentTimeMillis()))-17)) {
							JOptionPane.showMessageDialog(null,  "Funcionario deve ter a idade m�nima de 17 anos");
						}
						else 
							if( ano%4!=0 && mes==2 && dia>28) 
								JOptionPane.showMessageDialog(null,"Dia incopativel com o mes");
							else {
								txtdata.nextFocus();
								data="";
							}
				}catch(NumberFormatException nfe) { }
				
			}
		});
		panel.add(txtdata);
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(343, 74, 46, 20);
		panel.add(lblSexo);
		
		
		radFemenino = new JRadioButton("Femenino");
		lblSexo.setLabelFor(radFemenino);
		radFemenino.setSelected(true);
		radFemenino.setBounds(390, 73, 101, 29);
		panel.add(radFemenino);
		
		radMasculino = new JRadioButton("Masculino");
		radMasculino.setBounds(498, 73, 101, 29);
		panel.add(radMasculino);
		
		grp=new ButtonGroup();
		grp.add(radFemenino);
		grp.add(radMasculino);
		
		lblTipoDeFuncionrio = new JLabel("Tipo de Funcion\u00E1rio");
		lblTipoDeFuncionrio.setBounds(15, 119, 141, 20);
		panel.add(lblTipoDeFuncionrio);
		
		cbxTipoFuncionario = new JComboBox();
		
		cbxTipoFuncionario.setModel(new DefaultComboBoxModel(new String[] {"", "Operador de Revis\u00E3o", "Maquinista"}));
		cbxTipoFuncionario.setMaximumRowCount(3);
		cbxTipoFuncionario.setBounds(177, 116, 146, 26);
		panel.add(cbxTipoFuncionario);
		
		lblAnosDeExperiencia = new JLabel("Anos de Experiencia");
		lblAnosDeExperiencia.setBounds(15, 161, 146, 20);
		panel.add(lblAnosDeExperiencia);
		
		spnAnos = new JSpinner();
		spnAnos.setBounds(177, 158, 56, 26);
		panel.add(spnAnos);
		
		lblNumeroDeViagens = new JLabel("Numero de Viagens");
		lblNumeroDeViagens.setBounds(340, 116, 146, 20);
		panel.add(lblNumeroDeViagens);
		
		spnNrViagens = new JSpinner();
		spnNrViagens.setBounds(495, 113, 56, 26);

		panel.add(spnNrViagens);
		
		JLabel labelTitulo = new JLabel();
		labelTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitulo.setText(super.getTitle());
		labelTitulo.setBounds(0, 11, 627, 49);
		contentPane.add(labelTitulo);
		
		panel_1 = new JPanel();
		panel_1.setBounds(36, 283, 198, 135);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "Comboio", TitledBorder.CENTER, TitledBorder.ABOVE_TOP, null, new Color(0, 0, 0)));
		scrollPane.setBounds(0, 0, 198, 135);
		panel_1.add(scrollPane);
		
		
		
		preencherLista();
		String [] lista=new String [v.size()];
		Comboio cc;
		for(short a=0;a<v.size();a++) {
			cc=v.elementAt(a);
			lista[a]="Nr: "+cc.getNumero()+" Rota: "+cc.getRota().getNome();
		}
		
		list = new JList<String>();
		list.setSelectedIndex(1);
		DefaultListCellRenderer renderer =  (DefaultListCellRenderer)list.getCellRenderer();  
		renderer.setHorizontalAlignment(JLabel.CENTER);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setListData(new String[]{"Indisponivel"} );
//		list.setAlignmentX();
		list.setEnabled(false);
		list.setBackground(Color.lightGray);
		
		scrollPane.setViewportView(list);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(458, 299, 115, 29);
		btnSalvar.setEnabled(false);
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				registro();
			}
		});
		contentPane.add(btnSalvar);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setBounds(458, 344, 115, 29);
		contentPane.add(btnActualizar);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSair.setBounds(458, 389, 115, 29);
		contentPane.add(btnSair);
		
		
		//TODO: Listeners e Eventos
		
		//Combo Box Funcionario
		//TODO: Modificador de Edicao
		cbxTipoFuncionario.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				list.setListData(new String[]{"Indisponivel"} );
				renderer.setHorizontalAlignment(JLabel.CENTER);
				list.setBackground(Color.lightGray);
				list.setEnabled(false);
				lblAnosDeExperiencia.setEnabled(false);
				spnAnos.setEnabled(false);
				lblNumeroDeViagens.setEnabled(false);
				spnNrViagens.setEnabled(false);
				if(cbxTipoFuncionario.getSelectedIndex()==0) 
					btnSalvar.setEnabled(false);
				else {
					
				
					if(cbxTipoFuncionario.getSelectedIndex()==2) {//TODO: Para Maquinista
						lblAnosDeExperiencia.setEnabled(true);
						spnAnos.setEnabled(true);
						lblNumeroDeViagens.setEnabled(true);
						spnNrViagens.setEnabled(true);		
						list.setEnabled(true);
						list.setBackground(Color.WHITE);
						
						renderer.setHorizontalAlignment(JLabel.LEFT);
						list.setListData(lista);
						list.setSelectedIndex(0);
					}
					btnSalvar.setEnabled(true);
				}
				}
			
		});
		cbxTipoFuncionario.setSelectedIndex(0);
		
		//Spinner Numero de Viagens
		//TODO: Controle de Valores negativos
		spnNrViagens.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
					if((int)spnNrViagens.getValue()<0)
					spnNrViagens.setValue(0);
			}
			
		});
		
		//Spinner Anos de Experiencia
		//TODO: Controle de Valores negativos
		spnAnos.addChangeListener(new ChangeListener() {
					
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if((int)spnAnos.getValue()<0)
					spnAnos.setValue(0);
				try {
					DateFormat data=new SimpleDateFormat("dd/MM/yyyy");
					Date da=data.parse(txtdata.getText());
					Date data2=data.parse(data.format(System.currentTimeMillis()));
					long diff=data2.getTime()-da.getTime();
					byte days = (byte) ((diff / (1000*60*60*24))/365);
					int maxExp=(days-17);
					if((int)spnAnos.getNextValue()>(maxExp+1)) { 
						spnAnos.setValue(maxExp);
						JOptionPane.showMessageDialog(null, "De acordo com a idade, o m�ximo de anos de experiencia possivel �: "+maxExp,"Erro",JOptionPane.WARNING_MESSAGE);
					}
				}catch(ParseException pe) {JOptionPane.showMessageDialog(null, "Introduza a data de Nascimento: ");spnAnos.setValue(0);}
				
			}
		});
	}
	
	private void registro() {
		String sql,nome=validarString(txtNome.getText()),dataNasc=txtdata.getText(),plscript="";
		int anosEXp,nrViagens;
		int comboio;
		String genero="";
		try {
			DateFormat data=new SimpleDateFormat("dd/MM/yyyy");
			Date da=data.parse(dataNasc);
			Date data2=data.parse(data.format(System.currentTimeMillis()));
			long diff=data2.getTime()-da.getTime();
			byte days = (byte) ((diff / (1000*60*60*24))/365);
//			plscript="DECLARE \n\tl_id number:=0; \nBEGIN\n\tSELECT MAX(";
			
			if(radMasculino.isSelected())
				genero="M";
			else
				genero="F";
			if(nome.isEmpty() || da==null || genero.isEmpty())
				JOptionPane.showMessageDialog(null, "Erro ao adicionar\nVerifique os dados e tente novamente");
			else {
				plscript="DECLARE \n\tl_id operador_de_revisao.id_oper_revisao%TYPE; \nBEGIN\n\tSELECT MAX(";
				if(cbxTipoFuncionario.getSelectedIndex()==1) { //TODO: registrar Operador de Revisao				
					plscript+="id_oper_revisao)+1\nINTO \n\tl_id \nFROM \n\toperador_de_revisao; ";
					sql="\nINSERT INTO operador_de_revisao(id_oper_revisao,nome, idade, sexo) "
							+"\n\t\tVALUES (l_id,?,?,?); ";
					plscript+=sql+"\nEND;";
//					System.out.println(plscript);
					ps=BaseDados.conectar().prepareStatement(plscript);
					ps.setString(1, nome);
					ps.setByte(2, days);
					ps.setString(3, genero);
					ps.execute();
				}
				else {					
					plscript="DECLARE \n\tl_id maquinista.id_maquinista%TYPE; \nBEGIN\n\tSELECT MAX(";
					plscript+="id_maquinista)+1\n INTO\n\tl_id\n FROM\n\tmaquinista;";
					sql="\nINSERT INTO maquinista(id_maquinista,nome,idade,sexo,anos_exp,nr_viagens)"
							+"\n\tVALUES(l_id,?,?,?,?,?);\n"
							+ "INSERT INTO comboio_maquinista(id_comboio,id_maquinista)"
							+"\nVALUES(?,l_id);";
					plscript+=sql+ "\nEND;";
					anosEXp=(int) spnAnos.getValue();
					nrViagens=(int)spnNrViagens.getValue();
					comboio=v.elementAt(list.getSelectedIndex()).getId_comboio();
					ps=BaseDados.conectar().prepareStatement(plscript);
					ps.setString(1, nome);
//					System.out.println(nome);					
					ps.setByte(2, days);
//					System.out.println(days);
					ps.setString(3, genero);
//					System.out.println(genero);
					ps.setInt(4, anosEXp);
//					System.out.println(anosEXp);
					ps.setInt(5, nrViagens);
//					System.out.println(nrViagens);
					ps.setInt(6, comboio);
//					System.out.println(comboio);
					System.out.println(plscript);
					ps.execute();
								
					
				}
				JOptionPane.showMessageDialog(null, cbxTipoFuncionario.getSelectedItem()+" \""+nome+"\" adicionado com sucesso");
			}
		}catch(ParseException pe) {JOptionPane.showMessageDialog(null, "Erro ao adicionar\nData introduzida � inv�lida: "+pe.getMessage());}
		catch(SQLException sqlE) {JOptionPane.showMessageDialog(null, "Erro ao adicionar novo funcion�rio: "+sqlE.getMessage());}
	}
	private void preencherLista() {
		v.removeAllElements();
		int id_comboio,nr_vagoes,id_rota,movimento,lotacao;
		short nr_comboio;
		String nome_rota;
		Comboio c;
		Rota r;
		try {
			String sql="SELECT c.*, r.nome FROM comboio c JOIN rota r ON c.id_rota=r.id_rota";
			ps=BaseDados.conectar().prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()) {
				id_comboio=rs.getInt(1);
				nr_comboio=rs.getShort(2);
				lotacao= rs.getInt(3);
				nr_vagoes=rs.getInt(4);
				movimento=rs.getInt(5);
				id_rota=rs.getInt(6);
				nome_rota=rs.getString(7);
				c=new Comboio();
				c.setId_comboio(id_comboio);
				c.setNumero( nr_comboio);
				c.setLotacao(lotacao);
				c.setNr_vagoes(nr_vagoes);
				c.setEm_movimento(movimento);
				r=new Rota();
				r.setId_rota(id_rota);
				r.setNome(nome_rota);
				c.setRota(r);
				v.addElement(c);
				v.trimToSize();
//				JOptionPane.showMessageDialog(null, c.toString());
			}
		}catch(SQLException sqlE) {JOptionPane.showMessageDialog(null, "Erro listar comboios e rotas: "+sqlE.getMessage());}
		
	}
	private String validarString(String texto) {
		String aa=texto;
		try {
			if(aa.trim().length()<5) { 
				JOptionPane.showMessageDialog(null, "Nome Invalido","Erro",JOptionPane.ERROR_MESSAGE);
				aa="";
			}
		}catch(Exception ioe) {JOptionPane.showMessageDialog(null, ioe.getMessage());}
		return aa;
	}
}
