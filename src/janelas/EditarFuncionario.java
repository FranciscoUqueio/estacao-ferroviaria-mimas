package janelas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import conexao.BaseDados;
import modelos.Funcionario;
import modelos.Maquinista;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;

public class EditarFuncionario extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JPanel panSouth;
	private JButton okButton;
	private JButton cancelButton;
	private JLabel lblTitulo_1;
	private JLabel lblNome;
	private JLabel lblAnosDeExperiencia;
	private JLabel lblNumeroDeViagens;
	private JLabel lblIdade;
	private JTextField txtNome;
	private JFormattedTextField txtAnosExp;
	private JFormattedTextField txtNrViagens;
	private JFormattedTextField txtIdade;
	private MaskFormatter mask=new MaskFormatter("##");
	private PreparedStatement ps;
	private Funcionario f;
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) throws Exception{
//		try {
//			EditarFuncionario dialog = new EditarFuncionario(f);
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public EditarFuncionario(Funcionario f) throws Exception {
		this.f=f;
		setBounds(100, 100, 371, 295);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			lblTitulo_1 = new JLabel("Lista de Funcion\u00E1rios");
			lblTitulo_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblTitulo_1.setBounds(0, 0, 357, 49);
			contentPanel.add(lblTitulo_1);
		}
		{
			lblNome = new JLabel("Nome");
			lblNome.setHorizontalAlignment(SwingConstants.CENTER);
			lblNome.setBounds(10, 60, 134, 14);
			contentPanel.add(lblNome);
		}
		{
			lblAnosDeExperiencia = new JLabel("Anos de Experiencia");
			lblAnosDeExperiencia.setHorizontalAlignment(SwingConstants.CENTER);
			lblAnosDeExperiencia.setBounds(10, 103, 134, 14);
			contentPanel.add(lblAnosDeExperiencia);
		}
		
		lblNumeroDeViagens = new JLabel("Numero de Viagens");
		lblNumeroDeViagens.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumeroDeViagens.setBounds(10, 154, 134, 14);
		contentPanel.add(lblNumeroDeViagens);
		
		lblIdade = new JLabel("Idade");
		lblIdade.setHorizontalAlignment(SwingConstants.CENTER);
		lblIdade.setBounds(10, 205, 134, 14);
		contentPanel.add(lblIdade);
		
		txtNome = new JTextField();
		txtNome.setBounds(154, 57, 193, 20);
		contentPanel.add(txtNome);
		
		txtAnosExp = new JFormattedTextField(mask);
		txtAnosExp.setBounds(154, 100, 81, 20);
		contentPanel.add(txtAnosExp);
		
		txtNrViagens = new JFormattedTextField(new MaskFormatter("####"));
		txtNrViagens.setBounds(154, 151, 81, 20);
		contentPanel.add(txtNrViagens);
		
		txtIdade = new JFormattedTextField(mask);
		txtIdade.setEditable(false);
		txtIdade.setBounds(154, 202, 81, 20);
		contentPanel.add(txtIdade);
		
		
		panSouth = new JPanel();
		getContentPane().add(panSouth, BorderLayout.SOUTH);
		panSouth.setLayout(new GridLayout(0, 2, 0, 0));
			
		okButton = new JButton("Editar");
		okButton.setActionCommand("OK");
		okButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				actualizar();
			}
		});
		panSouth.add(okButton);
		getRootPane().setDefaultButton(okButton);
			
			
		cancelButton = new JButton("Cancelar");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int opc=JOptionPane.showConfirmDialog(null, "Cancelar Edicao?","Cancelar?",JOptionPane.YES_NO_OPTION);
				if(opc==JOptionPane.YES_OPTION) {
					try {
						dispose();
						new ListaFuncionario().setVisible(true);;
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		cancelButton.setActionCommand("Cancel");
		panSouth.add(cancelButton);
//		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				int opc=JOptionPane.showConfirmDialog(null, "Cancelar Edicao?","Cancelar?",JOptionPane.YES_NO_OPTION);
				if(opc==JOptionPane.YES_OPTION) {
					try {
						dispose();
						new ListaFuncionario().setVisible(true);;
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			
		});
		preencher();
//		setAlwaysOnTop(true);
	}


	private void actualizar() {
		// TODO Auto-generated method stub
		try {
			if(txtNome.getText().trim().isEmpty())
				if(f instanceof Maquinista) {
					String sql="UPDATE maquinista SET nome=?, anos_exp=?,nr_viagens=? WHERE id_maquinista="+f.getId_funcionario();
					System.out.println(sql);
					ps=BaseDados.conectar().prepareStatement(sql);
					ps.setString(1, txtNome.getText().trim());
					ps.setInt(2, Integer.parseInt(txtAnosExp.getText().trim()));
					ps.setInt(3, Integer.parseInt(txtNrViagens.getText().trim()));
//				ps.setInt(4, Integer.parseInt(txtIdade.getText()));
					ps.executeUpdate();
				}
				else {
					String sql="UPDATE operador_de_revisao SET nome='"+txtNome.getText().trim()+"' WHERE id_oper_revisao="+f.getId_funcionario();
					System.out.println(sql);
					ps=BaseDados.conectar().prepareStatement(sql);
//				ps.setString(1, txtNome.getText());
//				ps.setInt(2, Integer.parseInt(txtAnosExp.getText()));
//				ps.setInt(3, Integer.parseInt(txtNrViagens.getText()));
//				ps.setInt(4, Integer.parseInt(txtIdade.getText()));
					ps.executeUpdate();
				}
			else
				JOptionPane.showMessageDialog(null, "Preencha a caixa do Nome");
				
				JOptionPane.showMessageDialog(null, "Editado com sucesso");
				dispose();
				new ListaFuncionario();
					
		}catch(Exception sqlE) {JOptionPane.showMessageDialog(null, "Erro ao Actualizar: "+sqlE.getMessage());sqlE.printStackTrace();}
	}
	private void preencher() {
		this.txtNome.setText(f.getNome());
		this.txtIdade.setText(f.getIdade()+"");
		if(f instanceof Maquinista) {
			Maquinista mq=(Maquinista)f;
			this.txtAnosExp.setText(mq.getAnos_exp()+"");
			this.txtNrViagens.setText(mq.getNr_viagens()+"");
		}else {
			lblAnosDeExperiencia.setEnabled(false);
			txtAnosExp.setEnabled(false);
			lblNumeroDeViagens.setEnabled(false);
			txtNrViagens.setEnabled(false);
		}
			
		
	}
}
