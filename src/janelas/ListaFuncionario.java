package janelas;

import java.awt.EventQueue;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.border.MatteBorder;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

import conexao.BaseDados;
import modelos.Funcionario;
import modelos.Maquinista;
import modelos.Operador_de_Revisao;

import javax.swing.border.BevelBorder;
import javax.swing.ListSelectionModel;
import javax.swing.JSpinner;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class ListaFuncionario extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JSpinner spnIdade;
	private JTable table;
	private PreparedStatement ps;
	private ResultSet rs;
	private JButton btnSair;
	private JButton btnEditar;
	private JScrollPane scrollPane;
	private JPanel panResultados;
	private JPanel panFiltros;
	private JButton btnProcurar;
	private JLabel lblIdade;
	private JCheckBox chckbxMasculino;
	private JCheckBox chckbxFemenino;
	private JLabel lblSexo;
	private JLabel lblNome;
	private JLabel lblTitulo;
	private Vector<Funcionario> v=new Vector<Funcionario>();
	private String [] titulos=new String[] {"Tipo",	"Nome", "G\u00E9nero", "Idade", "Numero de Viagens", "Anos deExperi\u00EAncia"};
	private String dados[][]=new String[v.size()][titulos.length];
	private JLabel lblAnosExp;
	private JSpinner spnAnosXP;
	private JSpinner spnNrViagens;
	private JLabel lblNrViagens;
	private JLabel lblTipo;
	private JComboBox cbxTipo;
	private Maquinista mq;
	private Operador_de_Revisao opr;
	private JButton btnRemover;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					ListaFuncionario frame = new ListaFuncionario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public ListaFuncionario(String c) {}
	public ListaFuncionario() throws Exception {
		super("Lista de Funcionários");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 631, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		 lblTitulo = new JLabel("");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setText(super.getTitle());
		lblTitulo.setBounds(0, 11, 617, 49);
		contentPane.add(lblTitulo);
		
		panFiltros = new JPanel();
		panFiltros.setBorder(new TitledBorder(new MatteBorder(1, 1, 1, 1, new Color(0, 0, 0)), "Filtros", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panFiltros.setBounds(10, 71, 599, 99);
		contentPane.add(panFiltros);
		panFiltros.setLayout(null);
		
		 lblNome = new JLabel("Nome");
		lblNome.setBounds(15, 26, 41, 20);
		panFiltros.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(71, 26, 115, 20);
		panFiltros.add(txtNome);
		txtNome.setColumns(10);
		
		lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(25, 65, 31, 20);
		panFiltros.add(lblSexo);
		
		chckbxFemenino = new JCheckBox("Femenino");
		chckbxFemenino.setBounds(71, 65, 83, 20);
		panFiltros.add(chckbxFemenino);
		
		chckbxMasculino = new JCheckBox("Masculino");
		chckbxMasculino.setBounds(156, 65, 83, 20);
		panFiltros.add(chckbxMasculino);
		
		lblIdade = new JLabel("Idade");
		lblIdade.setBounds(337, 26, 41, 20);
		panFiltros.add(lblIdade);
		
		spnIdade = new JSpinner();
//		spnIdade.setValue(17);
		
		spnIdade.setBounds(374, 26, 37, 20);
		spnIdade.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if((int)spnIdade.getPreviousValue() ==15)
					spnIdade.setValue(0);
				if((int)spnIdade.getNextValue()==2)
					spnIdade.setValue(17);
			}
			
		});
		panFiltros.add(spnIdade);
		
		btnProcurar = new JButton("Procurar");
		btnProcurar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pesquisa();
				dados=preencher();
				table=new JTable(dados,titulos);
				for (int c = 0; c < table.getColumnCount(); c++)
				{
				    Class<?> col_class = table.getColumnClass(c);
				    table.setDefaultEditor(col_class, null);        // remove editor
				}
				table.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						editar();
					}
				});
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				scrollPane.setViewportView(table);
				
//				table.setEnabled(false);
				
			}
		});
		btnProcurar.setBounds(472, 61, 115, 29);
		panFiltros.add(btnProcurar);
		
		lblAnosExp = new JLabel("Anos de Experiencia(>=)");
		lblAnosExp.setBounds(245, 65, 144, 20);
		panFiltros.add(lblAnosExp);
		
		spnAnosXP = new JSpinner();
		spnAnosXP.setValue(-1);
		spnAnosXP.setBounds(399, 65, 48, 20);
		spnAnosXP.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if((int)spnAnosXP.getValue() < -1)
					spnAnosXP.setValue(-1);
			}
			
		});
		
		panFiltros.add(spnAnosXP);
		
		spnNrViagens = new JSpinner();
		spnNrViagens.setValue(-1);
		spnNrViagens.setBounds(552, 26, 37, 20);
		spnNrViagens.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
					if((int)spnNrViagens.getValue() < -1)
						spnNrViagens.setValue(-1);
			}
			
		});
		
		panFiltros.add(spnNrViagens);
		
		lblNrViagens = new JLabel("Numero de Viagens(>=)");
		lblNrViagens.setBounds(415, 26, 144, 20);
		panFiltros.add(lblNrViagens);
		
		lblTipo = new JLabel("Tipo");
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo.setBounds(196, 29, 49, 14);
		panFiltros.add(lblTipo);
		
		cbxTipo = new JComboBox();
		cbxTipo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(cbxTipo.getSelectedIndex()==0) {
					spnAnosXP.setValue(-1);
//					spnAnosXP.setEnabled(false);
					spnNrViagens.setValue(-1);
//					spnNrViagens.setEnabled(false);
				}
				else {
					spnAnosXP.setValue((int)spnAnosXP.getNextValue());
					spnNrViagens.setValue((int)spnNrViagens.getValue());
					spnAnosXP.setEnabled(true);
					spnNrViagens.setEnabled(true);
				}
			}
		});
		cbxTipo.setModel(new DefaultComboBoxModel(new String[] {"Operador de Revisao", "Maquinista"}));
		cbxTipo.setBounds(251, 25, 84, 22);
		panFiltros.add(cbxTipo);
		
		panResultados = new JPanel();
		panResultados.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), "Resultados", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, new Color(51, 51, 51)));
		panResultados.setBounds(16, 191, 593, 178);
		contentPane.add(panResultados);
		panResultados.setLayout(null);
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				if(!spnAnosXP.isFocusOwner() || !spnNrViagens.isFocusOwner())
					if( (int)spnAnosXP.getValue() == -1 && (int)spnNrViagens.getValue() == -1 )
						cbxTipo.setSelectedIndex(0);
					else {
						cbxTipo.setSelectedIndex(1);
						spnAnosXP.setValue((int)spnAnosXP.getValue());
						spnNrViagens.setValue((int)spnNrViagens.getValue());
					}
			}
			public void mouseClicked(MouseEvent e) {
				table.clearSelection();
				mq=null;
				opr=null;
			}
		});
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		scrollPane.setBounds(5, 19, 583, 115);
		panResultados.add(scrollPane);
		
		table = new JTable();
//		table.setEnabled(false);
		DefaultTableModel tableModel = new DefaultTableModel(dados, titulos) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		    
		};
		table.setModel(tableModel);
		
		table.setBorder(new MatteBorder(1, 1, 1, 1, new Color(0, 0, 0)));
		scrollPane.setViewportView(table);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(mq==null && opr==null) {
					JOptionPane.showMessageDialog(null, "Seleccione uma linha da Tabela para editar");
				}
				else {
					Funcionario f;
					try {
						if(cbxTipo.getSelectedIndex()==0)
							f=opr;
//							new EditarFuncionario(opr).setVisible(true);1
						else
							f=mq;
//							new EditarFuncionario(mq).setVisible(true);
						EditarFuncionario op=new EditarFuncionario(f);
						op.setVisible(true);
						dispose();
						

					} catch (Exception e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "Erro ao abrir a janela de edicao: "+e.getMessage());e.printStackTrace();
					}
				}
			}
		});
		btnEditar.setBounds(12, 140, 98, 26);
		panResultados.add(btnEditar);
		
		btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					if(mq==null && opr==null) 
						JOptionPane.showMessageDialog(null, "Seleccione uma linha da Tabela para remover");
					else { 
						int option=JOptionPane.showConfirmDialog(null, "Confirmar Remocao?","Remover",JOptionPane.YES_NO_OPTION);
						if(option==JOptionPane.YES_OPTION) {
							int id=v.get(table.getSelectedRow()).getId_funcionario();
							String sql;
							if(cbxTipo.getSelectedIndex()==0) 
								sql="DELETE FROM operador_de_revisao WHERE id_oper_revisao="+id;
							
							else {
								sql="DECLARE\n BEGIN\nDELETE FROM ";
								sql+="comboio_maquinista WHERE id_maquinista="+id;
								sql+=";\nDELETE FROM maquinista WHERE id_maquinista="+id;
								sql+=";\nEND;";
							}
//							System.out.println(sql);

							ps=BaseDados.conectar().prepareStatement(sql);
							ps.execute();
							BaseDados.conectar().commit();
							
							JOptionPane.showMessageDialog(null, v.get(table.getSelectedRow()).getNome()+" Removido com sucesso");
							
							pesquisa();
							dados=preencher();
							table=new JTable(dados,titulos);
							for (int c = 0; c < table.getColumnCount(); c++)
							{
							    Class<?> col_class = table.getColumnClass(c);
							    table.setDefaultEditor(col_class, null);        // remove editor
							}
							table.addMouseListener(new MouseAdapter() {
								public void mouseClicked(MouseEvent e) {
									editar();
								}
							});
							scrollPane.setViewportView(table);
							
						}
						else
							JOptionPane.showMessageDialog(null, "Operacao Cancelada");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					}
			}
		});
		btnRemover.setBounds(122, 140, 101, 26);
		panResultados.add(btnRemover);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSair.setBounds(496, 378, 98, 26);
		contentPane.add(btnSair);
	}
	
	private String [][] preencher(){
		String [][] dados=new String [v.size()][titulos.length];
		Operador_de_Revisao opr;
		Maquinista mq;
		int x=0;
		for (Funcionario fnc: v) {
			if (fnc instanceof Operador_de_Revisao) {
				opr=(Operador_de_Revisao)fnc;
				dados[x][0]="Operador de Revisao";
				dados[x][1]=opr.getNome();
				char gen=opr.getGenero();
				if(gen=='f' || gen=='F')
					dados[x][2]="Femenino";
				else
					dados[x][2]="Masculino";
				dados[x][3]=opr.getIdade()+"";
				dados[x][4]="-------------";
				dados[x][5]="-------------";
			}
			else {
				mq=(Maquinista)fnc;
				dados[x][0]="Maquinista";
				dados[x][1]=mq.getNome();
				char gen=mq.getGenero();
				if(gen=='f' || gen=='F')
					dados[x][2]="Femenino";
				else
					dados[x][2]="Masculino";
				dados[x][3]=mq.getIdade()+"";
				dados[x][4]=mq.getNr_viagens()+"";
				dados[x][5]=mq.getAnos_exp()+"";
			}
			x++;
		}
		return dados;
	}
	private void pesquisa() {
		v.removeAllElements();
//		Maquinista m;
//		Operador_de_Revisao opr;
		String generoM = "",generoF= "",nome="" ;
		int idade=(int)spnIdade.getValue(),anosXP=(int) spnAnosXP.getValue(),nrViagens=(int) spnNrViagens.getValue();

		if(chckbxFemenino.isSelected())
			generoF="f";
		if(chckbxMasculino.isSelected())
			generoM="m";
		try {
			String sql="SELECT * FROM ";
			nome=txtNome.getText();
			if(anosXP != -1 || nrViagens != -1) {
				sql+= "maquinista \nWHERE lower(nome) like '%"+nome+"%'\n"
						+ "AND (LOWER(sexo) LIKE '%"+generoF+"' OR LOWER(sexo) LIKE '%"+generoM+"')\n";
				if(anosXP!=-1)		
					sql+= "AND anos_exp>="+anosXP+" \n";
				if(nrViagens!=-1)
					sql+="AND nr_viagens>="+nrViagens+"\n";
				if(idade!=0) 
					sql+="AND idade>="+idade+"";
				
//				System.out.println(sql);
				ps=BaseDados.conectar().prepareStatement(sql);					
				rs=ps.executeQuery();
//				JOptionPane.showMessageDialog(null, rs.next()); 
				while(rs.next()) {
					
					mq=new Maquinista();
					mq.setId_funcionario(rs.getInt(1));
					mq.setNome(rs.getString(2));
					mq.setIdade(rs.getByte(3));
					mq.setGenero(rs.getString(4).charAt(0));
					mq.setAnos_exp(rs.getByte(5));
					mq.setNr_viagens(rs.getInt(6));
					v.addElement(mq);
//					JOptionPane.showMessageDialog(null, "Adicionado");
				}
			}
			else {
				sql+="operador_de_revisao\nWHERE lower(nome) like '%"+nome+"%'\n"
						+ "AND (LOWER(sexo) LIKE '%"+generoF+"' OR LOWER(sexo) LIKE '%"+generoM+"')\n";
				if(idade!=0) 
					sql+="AND idade>="+idade+"";
//				System.out.println(sql);
				ps=BaseDados.conectar().prepareStatement(sql);					
				rs=ps.executeQuery();
				while(rs.next()) {
					
					opr=new Operador_de_Revisao();
					opr.setId_funcionario(rs.getInt(1));
					opr.setNome(rs.getString(2));
					opr.setIdade(rs.getByte(3));
					opr.setGenero(rs.getString(4).charAt(0));
					v.addElement(opr);
					
				}
			}
		}catch(NumberFormatException nfe) {JOptionPane.showMessageDialog(null, nfe.getMessage());}
		catch(SQLException sqlE) {JOptionPane.showMessageDialog(null, sqlE.getMessage());sqlE.printStackTrace();}
	}
	private void editar() {
		if(cbxTipo.getSelectedIndex()==0) {
			opr=(Operador_de_Revisao) v.get(table.getSelectedRow());
//			JOptionPane.showMessageDialog(null, opr.toString());
		}
		else {
			mq=(Maquinista)v.get(table.getSelectedRow());
//			JOptionPane.showMessageDialog(null, mq.toString());
		}
	}
}
