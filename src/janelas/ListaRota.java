package janelas;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import conexao.BaseDados;
import modelos.Estacao;
import modelos.Rota;

import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
public class ListaRota extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField txtPrecoInicial;
	private JTextField txtPrecoFinal;
	private ResultSet rs;
	PreparedStatement ps;
	private Rota rota;
	private Vector<Rota> v;
	private String [][] dados;
	private String [] titulos = {"Nome","Estacao-Saida", "Estacao-Chegada", "Preco"};
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListaRota frame = new ListaRota();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListaRota() {
		setTitle("Lista de Rotas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 723, 511);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitulo = new JLabel("");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setText(super.getTitle());
		lblTitulo.setBounds(0, 0, 701, 60);
		contentPane.add(lblTitulo);
		
		JPanel panFiltro = new JPanel();
		panFiltro.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Filtros", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panFiltro.setBounds(102, 76, 497, 166);
		contentPane.add(panFiltro);
		panFiltro.setLayout(null);
		
		JLabel lblPreco = new JLabel("Pre\u00E7o");
		lblPreco.setHorizontalAlignment(SwingConstants.CENTER);
		lblPreco.setBounds(31, 16, 100, 25);
		panFiltro.add(lblPreco);
		
		txtPrecoInicial = new JTextField();
		txtPrecoInicial.setBounds(31, 46, 100, 25);
		panFiltro.add(txtPrecoInicial);
		txtPrecoInicial.setColumns(10);
//		Time t=new Time(System.currentTimeMillis());
//	
//		txtPrecoInicial.setText(t.valueOf("22:40:"+new SimpleDateFormat("ss").format(System.currentTimeMillis())+"")+"");
		JLabel label = new JLabel("<>");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(31, 76, 100, 25);
		panFiltro.add(label);
		
		txtPrecoFinal = new JTextField();
		txtPrecoFinal.setBounds(31, 104, 100, 25);
		panFiltro.add(txtPrecoFinal);
		txtPrecoFinal.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(294, 44, 170, 31);
		panFiltro.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(294, 100, 170, 31);
		panFiltro.add(comboBox_1);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon("C:\\Users\\Lenovo\\Documents\\Personal\\Works\\ISCTEM\\2o Ano\\1o Semestre\\POO2\\POO2_Workspace\\Estacao Ferroviaria\\index.png"));
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setBounds(294, 74, 170, 25);
		panFiltro.add(label_1);
		
		JLabel lblEstaes = new JLabel("Esta\u00E7\u00F5es");
		lblEstaes.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstaes.setBounds(294, 16, 170, 25);
		panFiltro.add(lblEstaes);
		
		JLabel lblPartida = new JLabel("Partida");
		lblPartida.setBounds(210, 45, 69, 25);
		panFiltro.add(lblPartida);
		
		JLabel lblChegada = new JLabel("Chegada");
		lblChegada.setBounds(210, 103, 69, 25);
		panFiltro.add(lblChegada);
		
		JPanel panResultado = new JPanel();
		panResultado.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "Resultado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panResultado.setBounds(15, 258, 671, 148);
		contentPane.add(panResultado);
		panResultado.setLayout(new GridLayout(1, 0, 0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panResultado.add(scrollPane);
		
		v = new Vector<Rota>();
		pullDataFromDB();
		fillTable();
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(new DefaultTableModel(dados,titulos));
		scrollPane.setViewportView(table);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(571, 410, 115, 29);
		btnSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
			}
		});
		contentPane.add(btnSair);
	}
	
	public void pullDataFromDB() {
		String sql="SELECT * FROM Rota@local";
		try {
			ps = BaseDados.conectar().prepareStatement(sql);
			rs = ps.executeQuery();
			createObjeEstacao(rs);
			//System.out.println("Entrei no pull");
		}catch(SQLException s) {JOptionPane.showMessageDialog(null,s);}
	}
	
	public void createObjeEstacao(ResultSet resultSet) {
		try{
			while(resultSet.next()) {
				rota = new Rota();
				rota.setId_rota(resultSet.getInt(1));
				rota.setEstacao_saida(resultSet.getString(2));
				rota.setEstacao_chegada(resultSet.getString(3));
				rota.setPreco(resultSet.getFloat(4));
				rota.setNome(resultSet.getString(5));
				rota.setEstacao(resultSet.getInt(6));
				v.addElement(rota);
			}
			//System.out.println("Entrei no create");
		}catch(SQLException s){JOptionPane.showMessageDialog(null,s);}
	}
	
	public void fillTable() {
	    dados = new String[v.size()][titulos.length];
	    for(int i=0;i<dados.length;i++) {
		    rota = v.elementAt(i);
	    	dados[i][0]= rota.getNome();
	    	dados[i][1]= rota.getEstacao_saida();
	    	dados[i][2]= rota.getEstacao_chegada();
	    	dados[i][3]= rota.getPreco()+"";
 	    }
		//System.out.println("Entrei no fill");
	}
}
