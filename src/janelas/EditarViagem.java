package janelas;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.MaskFormatter;

import conexao.BaseDados;
import modelos.Comboio;
import modelos.Rota;
import modelos.Viagem;

import java.awt.GridLayout;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JSlider;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class EditarViagem extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JComboBox cbxComboio;
	private JTextField txtEstacaoDePartida;
	private JTextField txtEstacaoDeChegada;
	private JSlider sldPassageiros;
	private JLabel lblNumeroDePassageiros;
	private JLabel lblEstacaoDeChegada;
	private JLabel lblEstacaoDePartida;
	private JLabel lblRota;
	private JLabel lblComboio;
	private JFormattedTextField txtdata;
	private JLabel lblData;
	private JLabel label;
	private Viagem v;
	private JButton okButton;
	private JButton cancelButton;
	private JLabel spnTxt;
	protected int cbxind=0;
	protected Vector<Comboio> vComboio=new Vector<Comboio>();
	private PreparedStatement ps;
	private ResultSet rs;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			EditarViagem dialog = new EditarViagem(new Viagem());
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//			dialog.setAlwaysOnTop(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 * @throws Exception 
	 */
	public EditarViagem(Viagem v) throws Exception {
		this.v=v;
		setTitle("Editar Viagem");
		setBounds(100, 100, 659, 272);
		getContentPane().setLayout(new BorderLayout());
//		setAlwaysOnTop(true);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			label = new JLabel(super.getTitle().toUpperCase());
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setBounds(0, 0, 645, 44);
			contentPanel.add(label);
		}
		{
			lblData = new JLabel("Data");
			lblData.setHorizontalAlignment(SwingConstants.CENTER);
			lblData.setBounds(10, 78, 142, 28);
			contentPanel.add(lblData);
		}
		{
			txtdata = new JFormattedTextField(new MaskFormatter("##/##/2020"));
			txtdata.setEditable(false);
			txtdata.setText(this.v.getData_viagem());
			txtdata.setHorizontalAlignment(SwingConstants.CENTER);
			txtdata.addKeyListener(new KeyAdapter() {
				public void keyReleased(KeyEvent e) {				
					String data=txtdata.getText();
					boolean a=true;
//					System.out.println("Data: "+data);
					try {
						byte dia=Byte.parseByte(data.substring(0, 2));
//						System.out.println("Dia: "+dia);
						if(dia<0 || dia>31) {
							JOptionPane.showMessageDialog(null, "Dia Invalido");
							txtdata.setText("");						
						}
						byte mes=Byte.parseByte(data.substring(3,5));
						if(mes<1 || mes>12) {
							JOptionPane.showMessageDialog(null, "Mes Invalido");
							if(dia<10)
								txtdata.setText("0"+dia);				
							else
								txtdata.setText(""+dia);		
							a=false;
						}else
							switch(mes) {
							case 4: case 6: case 9:
							case 11:
								if(dia<0 || dia>30) {
									JOptionPane.showMessageDialog(null, "Dia Invalido\n Dia incopativel com o mes");
									txtdata.setText("");		
									a=false;
								}
								break;
							case 2: 
								int ano=Integer.parseInt(data.substring(6,10));
								if((dia<0 || dia>29) && (ano%4!=0)) {
									JOptionPane.showMessageDialog(null, "Dia Invalido\n Dia incopativel com o mes");
									txtdata.selectAll();	
									a=false;
								}		
								break;
							}
						if(a==true)
							lblComboio.transferFocus();
						else
							txtdata.selectAll();					
					}catch(NumberFormatException nfe) { }
					
				}
			});
			txtdata.setBounds(162, 81, 156, 22);
			contentPanel.add(txtdata);
		}
		{
			lblComboio = new JLabel("Comboio");
			lblComboio.setHorizontalAlignment(SwingConstants.CENTER);
			lblComboio.setBounds(10, 117, 142, 28);
			contentPanel.add(lblComboio);
		}
		
		{
			lblRota = new JLabel("Rota");
			lblRota.setHorizontalAlignment(SwingConstants.CENTER);
			lblRota.setBounds(330, 56, 298, 28);
			contentPanel.add(lblRota);
		}
		{
			lblEstacaoDePartida = new JLabel("Estacao de Partida");
			lblEstacaoDePartida.setHorizontalAlignment(SwingConstants.CENTER);
			lblEstacaoDePartida.setBounds(330, 78, 144, 28);
			contentPanel.add(lblEstacaoDePartida);
		}
		{
			txtEstacaoDePartida = new JTextField();
			txtEstacaoDePartida.setText("Estacao de Partida");
			txtEstacaoDePartida.setHorizontalAlignment(SwingConstants.CENTER);
			txtEstacaoDePartida.setEditable(false);
			txtEstacaoDePartida.setBounds(484, 82, 156, 20);
			contentPanel.add(txtEstacaoDePartida);
		}
		{
			lblEstacaoDeChegada = new JLabel("Estacao de Chegada");
			lblEstacaoDeChegada.setHorizontalAlignment(SwingConstants.CENTER);
			lblEstacaoDeChegada.setBounds(330, 117, 144, 28);
			contentPanel.add(lblEstacaoDeChegada);
		}
		{
			txtEstacaoDeChegada = new JTextField();
			txtEstacaoDeChegada.setText("Estacao de Chegada");
			txtEstacaoDeChegada.setHorizontalAlignment(SwingConstants.CENTER);
			txtEstacaoDeChegada.setEditable(false);
			txtEstacaoDeChegada.setBounds(484, 121, 156, 20);
			contentPanel.add(txtEstacaoDeChegada);
		}
		{
			lblNumeroDePassageiros = new JLabel("Numero de Passageiros");
			lblNumeroDePassageiros.setHorizontalAlignment(SwingConstants.CENTER);
			lblNumeroDePassageiros.setBounds(10, 156, 144, 28);
			contentPanel.add(lblNumeroDePassageiros);
		}
		
		{
			spnTxt = new JLabel();
			spnTxt.setHorizontalAlignment(SwingConstants.CENTER);
			spnTxt.setBounds(330, 163, 31, 14);
			contentPanel.add(spnTxt);
		}
		
		{
			sldPassageiros = new JSlider();
			sldPassageiros.setPaintTicks(true);
			sldPassageiros.setMinimum(-1);
			sldPassageiros.setMaximum(300);
			sldPassageiros.setMajorTickSpacing(100);
			sldPassageiros.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					// TODO Auto-generated method stub
					if(sldPassageiros.getValue()!=-1)
					spnTxt.setText(sldPassageiros.getValue()+"");
					else
						spnTxt.setText(" ");
				}
			});
			sldPassageiros.setValue(v.getNr_passageiros());
			sldPassageiros.setBounds(164, 151, 156, 45);
			contentPanel.add(sldPassageiros);
		}
	
		{
			preencherComboio();
			String s[]=new String[vComboio.size()+1];
			int x=1;
			s[0]=" ";
//			System.out.println(v.size());
			for(Comboio vv: vComboio) {
//				System.out.println(vv.toString());
				s[x]=vv.getNumero()+" - "+vv.getRota().getNome();
				x++;
			}
			cbxComboio = new JComboBox(s);
			cbxComboio.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					// TODO Auto-generated method stub
					cbxind=cbxComboio.getSelectedIndex();
					if(cbxind==0) {
//						btnSalvar.setEnabled(false);
						txtEstacaoDePartida.setText("Estacao de Partida");
						txtEstacaoDeChegada.setText("Estacao de Chegada");
					}
					else {
//						System.out.println(vComboio.elementAt(cbxind-1).toString());
						txtEstacaoDePartida.setText(vComboio.elementAt(cbxind-1).getRota().getEstacao_saida());
						txtEstacaoDeChegada.setText(vComboio.elementAt(cbxind-1).getRota().getEstacao_chegada());			
						sldPassageiros.setMaximum(vComboio.elementAt(cbxind-1).getLotacao());
					}
				}
			});
			int pos=0;
			for(Comboio cmb: vComboio) {
				if(cmb.getId_comboio()==v.getComboio().getId_comboio()) {
					cbxComboio.setSelectedIndex(pos);
//					cbxind=pos;
					break;
				}
				pos++;
			}
			cbxComboio.setBounds(162, 120, 156, 22);
			contentPanel.add(cbxComboio);
		}
		{
			JPanel panSouth = new JPanel();
			getContentPane().add(panSouth, BorderLayout.SOUTH);
			panSouth.setLayout(new GridLayout(0, 2, 0, 0));
			{
				okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						actualizar();
					}
				});
				panSouth.add(okButton);
				getRootPane().setDefaultButton(okButton);
					
					
				cancelButton = new JButton("Cancelar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						int opc=JOptionPane.showConfirmDialog(contentPanel, "Cancelar Edicao?","Cancelar?",JOptionPane.YES_NO_OPTION);
						if(opc==JOptionPane.YES_OPTION) {
							try {
								dispose();
								new ListaViagem().setVisible(true);;
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
				});
				cancelButton.setActionCommand("Cancel");
				panSouth.add(cancelButton);
			}
//				setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			setLocationRelativeTo(contentPanel);
				addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosing(WindowEvent e) {
						// TODO Auto-generated method stub
						int opc=JOptionPane.showConfirmDialog(contentPanel, "Cancelar Edicao?","Cancelar?",JOptionPane.YES_NO_OPTION);
						if(opc==JOptionPane.YES_OPTION) {
							try {
								dispose();
								new ListaViagem().setVisible(true);;
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
					
				});
				setAlwaysOnTop(true);
			}
		addWindowFocusListener(new WindowAdapter() {
			
			@Override
			public void windowLostFocus(WindowEvent e) {
				// TODO Auto-generated method stub
//				txtdata.requestFocus();
				toFront();
				try {
					new ListaViagem().dispose();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
//		addMouseListener(new MouseListener() {
//					
//					@Override
//					public void mouseReleased(MouseEvent arg0) {
//						// TODO Auto-generated method stub
//						
//					}
//					
//					@Override
//					public void mousePressed(MouseEvent arg0) {
//						// TODO Auto-generated method stub
//						
//					}
//					
//					@Override
//					public void mouseExited(MouseEvent arg0) {
//						// TODO Auto-generated method stub
////						JOptionPane.showMessageDialog(null, "Erro");
//						getContentPane().requestFocus();
//					}
//					
//					@Override
//					public void mouseEntered(MouseEvent arg0) {
//						// TODO Auto-generated method stub
//						
//					}
//					
//					@Override
//					public void mouseClicked(MouseEvent arg0) {
//						// TODO Auto-generated method stub
//						getContentPane().requestFocus();
//					}
//				});
				
			
	}
	private void actualizar() {
		// TODO Auto-generated method stub
		try {		
		String sql="UPDATE viagem SET data_viagem=?,nr_passageiros=?,id_comboio=?"
				+ "\nWHERE id_comboio="+v.getComboio().getId_comboio();
			ps=BaseDados.conectar().prepareStatement(sql);
			ps.setString(1, txtdata.getText());
			ps.setInt(2, sldPassageiros.getValue());
			ps.setInt(3, vComboio.get(cbxind-1).getId_comboio());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao Actualizar: "+e.getMessage());
		}
		
	}
	private void preencherComboio() {
		vComboio.removeAllElements();
//		Viagem vv;
		Comboio c;
		Rota r;
		try {
			String sql="SELECT c.id_comboio,c.numero,c.lotacao,r.id_rota,r.nome,r.estacao_saida,r.estacao_chegada"
					+ "\nFROM comboio c  JOIN rota r ON c.id_rota=r.id_rota";
//			System.out.println(sql);
			ps=BaseDados.conectar().prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()) {
				c=new Comboio();
				c.setId_comboio(rs.getInt(1));
				c.setNumero(rs.getShort(2));
				c.setLotacao(rs.getInt(3));
				r=new Rota();
				r.setId_rota(rs.getInt(4));
				r.setNome(rs.getString(5));
				r.setEstacao_saida(rs.getString(6));
				r.setEstacao_chegada(rs.getString(7));
				c.setRota(r);
				vComboio.addElement(c);
			}
			
		} catch (SQLException e) {JOptionPane.showMessageDialog(null, "Erro ao adicionar Viagem: "+e.getMessage());
		}		
	}

}
