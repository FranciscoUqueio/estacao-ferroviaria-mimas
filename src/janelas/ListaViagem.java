package janelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

import conexao.BaseDados;
import modelos.Comboio;
import modelos.Estacao;
import modelos.Maquinista;
import modelos.Operador_de_Revisao;
import modelos.Rota;
import modelos.Viagem;

import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import java.awt.Color;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JSlider;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.Box;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ListaViagem extends JFrame {

	private JPanel contentPane;
	private JLabel lblTitulo;
	private JPanel panel;
	private JLabel lblData;
	private JFormattedTextField txtdata;
	private JLabel lblComboio;
	private JComboBox cbxComboio;
	private JLabel lblEstacaoDePartida;
	private JLabel lblEstacaoDeChegada;
	private JTextField txtEstacaoDePartida;
	private JTextField txtEstacaoDeChegada;
	private JLabel lblRota;
	private JLabel lblNumeroDePassageiros;
	private JSlider sldPassageiros;
	private JLabel spnTxt;
	protected int cbxind=0;
	protected Vector<Viagem> v=new Vector<Viagem>();
	private PreparedStatement ps;
	private ResultSet rs;
	private JPanel panResultados;
	private JScrollPane scrollPane;
	private JTable table;
	private String[] titulos= {"Data","Comboio","Rota","Estacao Saida","Hora Saida","Estacao Chegada","Hora Chegada","Passageiros"};
	private String[][] dados=new String[v.size()][titulos.length];
	private JButton btnPesquisar;
	private JButton btnEditar;
	private JButton btnRemover;
	private JButton btnSair;
	private Viagem vv;
	private Vector<Comboio> vComboio=new Vector<Comboio>();
//	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListaViagem frame = new ListaViagem();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * String sql="select c.numero,r.* from comboio c JOIN rota r on r.id_rota=c.id_rota JOIN Comboio_Maquinista m on\r\n" + 
				"c.id_comboio=m.id_comboio;";
	 * @throws Exception 
	 */
	public ListaViagem() throws Exception {
		setTitle("Lista de Viagem");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 792, 581);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblTitulo = new JLabel(super.getTitle());
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(0, 12, 778, 48);
		contentPane.add(lblTitulo);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Filtros", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(37, 73, 701, 162);
		contentPane.add(panel);
		panel.setLayout(null);
		
		lblData = new JLabel("Data");
		lblData.setHorizontalAlignment(SwingConstants.CENTER);
		lblData.setBounds(10, 33, 142, 28);
		panel.add(lblData);
		
		txtdata = new JFormattedTextField(new MaskFormatter("##/##/2020"));
		txtdata.setHorizontalAlignment(SwingConstants.CENTER);
		txtdata.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {				
				String data=txtdata.getText();
				boolean a=true;
//				System.out.println("Data: "+data);
				try {
					byte dia=Byte.parseByte(data.substring(0, 2));
//					System.out.println("Dia: "+dia);
					if(dia<0 || dia>31) {
						JOptionPane.showMessageDialog(null, "Dia Invalido");
						txtdata.setText("");						
					}
					byte mes=Byte.parseByte(data.substring(3,5));
					if(mes<1 || mes>12) {
						JOptionPane.showMessageDialog(null, "Mes Invalido");
						if(dia<10)
							txtdata.setText("0"+dia);				
						else
							txtdata.setText(""+dia);		
						a=false;
					}else
						switch(mes) {
						case 4: case 6: case 9:
						case 11:
							if(dia<0 || dia>30) {
								JOptionPane.showMessageDialog(null, "Dia Invalido\n Dia incopativel com o mes");
								txtdata.setText("");		
								a=false;
							}
							break;
						case 2: 
							int ano=Integer.parseInt(data.substring(6,10));
							if((dia<0 || dia>29) && (ano%4!=0)) {
								JOptionPane.showMessageDialog(null, "Dia Invalido\n Dia incopativel com o mes");
								txtdata.selectAll();	
								a=false;
							}		
							break;
						}
					if(a==true)
						lblComboio.transferFocus();
					else
						txtdata.selectAll();					
				}catch(NumberFormatException nfe) { }
				
			}
		});
		txtdata.setBounds(162, 36, 156, 22);
		panel.add(txtdata);
		
		lblComboio = new JLabel("Comboio");
		lblComboio.setHorizontalAlignment(SwingConstants.CENTER);
		lblComboio.setBounds(10, 72, 142, 28);
		panel.add(lblComboio);
		
		preencherComboio();
		String s[]=new String[vComboio.size()+1];
		int x=1;
		s[0]=" ";
//		System.out.println(v.size());
		for(Comboio vv: vComboio) {
//			System.out.println(vv.toString());
			s[x]=vv.getNumero()+" - "+vv.getRota().getNome();
			x++;
		}
		cbxComboio = new JComboBox(s);
		cbxComboio.setBounds(162, 75, 156, 22);
		cbxComboio.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				cbxind=cbxComboio.getSelectedIndex();
				if(cbxind==0) {
//					btnSalvar.setEnabled(false);
					txtEstacaoDePartida.setText("Estacao de Partida");
					txtEstacaoDeChegada.setText("Estacao de Chegada");
				}
				else {
					txtEstacaoDePartida.setText(vComboio.elementAt(cbxind-1).getRota().getEstacao_saida());
					txtEstacaoDeChegada.setText(vComboio.elementAt(cbxind-1).getRota().getEstacao_chegada());			
					sldPassageiros.setMaximum(vComboio.elementAt(cbxind-1).getLotacao());
				}
			}
		});
		panel.add(cbxComboio);
		
		lblEstacaoDePartida = new JLabel("Estacao de Partida");
		lblEstacaoDePartida.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstacaoDePartida.setBounds(364, 33, 144, 28);
		panel.add(lblEstacaoDePartida);
		
		lblEstacaoDeChegada = new JLabel("Estacao de Chegada");
		lblEstacaoDeChegada.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstacaoDeChegada.setBounds(364, 72, 144, 28);
		panel.add(lblEstacaoDeChegada);
		
		txtEstacaoDePartida = new JTextField();
		txtEstacaoDePartida.setText("Estacao de Partida");
		txtEstacaoDePartida.setHorizontalAlignment(SwingConstants.CENTER);
		txtEstacaoDePartida.setEditable(false);
		txtEstacaoDePartida.setBounds(518, 37, 156, 20);
		panel.add(txtEstacaoDePartida);
		txtEstacaoDePartida.setColumns(10);
		
		txtEstacaoDeChegada = new JTextField();
		txtEstacaoDeChegada.setText("Estacao de Chegada");
		txtEstacaoDeChegada.setHorizontalAlignment(SwingConstants.CENTER);
		txtEstacaoDeChegada.setEditable(false);
		txtEstacaoDeChegada.setColumns(10);
		txtEstacaoDeChegada.setBounds(518, 76, 156, 20);
		panel.add(txtEstacaoDeChegada);
		lblRota = new JLabel("Rota");
		lblRota.setHorizontalAlignment(SwingConstants.CENTER);
		lblRota.setBounds(364, 11, 298, 28);
		panel.add(lblRota);
		
		lblNumeroDePassageiros = new JLabel("Numero de Passageiros");
		lblNumeroDePassageiros.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumeroDePassageiros.setBounds(10, 111, 144, 28);
		panel.add(lblNumeroDePassageiros);
		
		sldPassageiros = new JSlider();
		sldPassageiros.setPaintTicks(true);
		sldPassageiros.setMinimum(-1);
		sldPassageiros.setMajorTickSpacing(100);
		sldPassageiros.setValue(0);
		sldPassageiros.setMaximum(300);
		sldPassageiros.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				// TODO Auto-generated method stub
				if(sldPassageiros.getValue()!=-1)
				spnTxt.setText(sldPassageiros.getValue()+"");
				else
					spnTxt.setText(" ");
			}
		});
		sldPassageiros.setBounds(164, 106, 156, 45);
		panel.add(sldPassageiros);
		
		spnTxt = new JLabel();
		spnTxt.setHorizontalAlignment(SwingConstants.CENTER);
		spnTxt.setBounds(330, 128, 31, 14);
		panel.add(spnTxt);
		
		btnPesquisar = new JButton("Procurar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				filtro();
				
				table=new JTable(dados,titulos);
				for (int c = 0; c < table.getColumnCount(); c++)
				{
				    Class<?> col_class = table.getColumnClass(c);
				    table.setDefaultEditor(col_class, null);        // remove editor
				}
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				table.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						editar();
					}
				});
				scrollPane.setViewportView(table);
			}
		});
		btnPesquisar.setBounds(374, 122, 298, 28);
		panel.add(btnPesquisar);
		
		panResultados = new JPanel();
		panResultados.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Resultados", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panResultados.setBounds(10, 256, 756, 231);
		contentPane.add(panResultados);
		panResultados.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPane.setBounds(7, 16, 741, 176);
		panResultados.add(scrollPane);
		
		table = new JTable();
		DefaultTableModel tableModel = new DefaultTableModel(dados, titulos) {
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		table.setModel(tableModel);
		
		table.setBorder(new MatteBorder(1, 1, 1, 1, new Color(0, 0, 0)));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
//					System.out.println(vv.toString());
					if(vv==null)
						JOptionPane.showMessageDialog(contentPane, "Selecione uma linha da tabela");
					else {
						new EditarViagem(vv).setVisible(true);
						dispose();
					}
						
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnEditar.setBounds(71, 193, 300, 26);
		panResultados.add(btnEditar);
		
		btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(vv==null) 
						JOptionPane.showMessageDialog(null, "Seleccione uma linha da Tabela para remover");
					else { 
						int option=JOptionPane.showConfirmDialog(null, "Confirmar Remocao?","Remover",JOptionPane.YES_NO_OPTION);
						if(option==JOptionPane.YES_OPTION) {
							int id=v.get(table.getSelectedRow()).getId_viagem();
							System.out.println(id);
							String sql="DELETE FROM viagem WHERE id_viagem="+id;
							System.out.println(sql);
							ps=BaseDados.conectar().prepareStatement(sql);
							System.out.println("Prepared");
							ps.execute();
							System.out.println("Executed");
							BaseDados.conectar().commit();
							System.out.println("Commit");
							JOptionPane.showMessageDialog(null, "Viagem Removida com sucesso");
							
							filtro();
							table=new JTable(dados,titulos);
							for (int c = 0; c < table.getColumnCount(); c++)
							{
							    Class<?> col_class = table.getColumnClass(c);
							    table.setDefaultEditor(col_class, null);        // remove editor
							}
							table.addMouseListener(new MouseAdapter() {
								public void mouseClicked(MouseEvent e) {
									editar();
								}
							});
							scrollPane.setViewportView(table);
							
						}
						else
							JOptionPane.showMessageDialog(null, "Operacao Cancelada");
					}
					
				} catch (SQLException sqlE) {JOptionPane.showMessageDialog(contentPane, "Erro ao remover: "+sqlE.getMessage());}
			
				
			}
		});
		btnRemover.setBounds(383, 193, 300, 26);
		panResultados.add(btnRemover);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSair.setBounds(637, 506, 101, 26);
		contentPane.add(btnSair);
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				table.clearSelection();
				vv=null;
			}
		});
		setLocationRelativeTo(contentPane);
		
	}
	private void preencherComboio() {
		vComboio.removeAllElements();
//		Viagem vv;
		Comboio c;
		Rota r;
		try {
			String sql="SELECT c.id_comboio,c.numero,c.lotacao,r.id_rota,r.nome,r.estacao_saida,r.estacao_chegada"
					+ "\nFROM comboio c  JOIN rota r ON c.id_rota=r.id_rota";
//			System.out.println(sql);
			ps=BaseDados.conectar().prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()) {
				c=new Comboio();
				c.setId_comboio(rs.getInt(1));
				c.setNumero(rs.getShort(2));
				c.setLotacao(rs.getInt(3));
				r=new Rota();
				r.setId_rota(rs.getInt(4));
				r.setNome(rs.getString(5));
				r.setEstacao_saida(rs.getString(6));
				r.setEstacao_chegada(rs.getString(7));
				c.setRota(r);
				vComboio.addElement(c);
			}
			
		} catch (SQLException e) {JOptionPane.showMessageDialog(null, "Erro ao adicionar Viagem: "+e.getMessage());
		}		
	}
	private void filtro() {
		v.removeAllElements();
		int  nrPassageiros=-1;
		try {
			String data=txtdata.getText();
			nrPassageiros=sldPassageiros.getValue();
			String sql="SELECT v.id_viagem,v.hora_Saida,v.hora_chegada,v.nr_passageiros,v.data_viagem,c.id_comboio,c.numero,"
					+ "c.lotacao,r.id_rota,r.nome,r.estacao_saida,r.estacao_chegada\n" + 
					"FROM viagem v JOIN comboio c ON v.id_comboio=c.id_comboio JOIN rota r ON c.id_rota=r.id_rota"
					+ "\nWHERE id_viagem!=0";
			if( ! data.trim().equalsIgnoreCase("/  /2020"))
				sql+="\nAND v.data_viagem='"+data+"'";
			if(cbxind-1!=-1)
				sql+="\nAND v.id_comboio="+vComboio.get(cbxind-1).getId_comboio();
			if(nrPassageiros!=-1)
				sql+="\nAND v.nr_passageiros>="+nrPassageiros;
			sql+="\nORDER BY nr_passageiros ASC";
//			System.out.println(sql);
			ps=BaseDados.conectar().prepareStatement(sql);
			rs=ps.executeQuery();
				while(rs.next()) {
					vv=new Viagem();
					vv.setId_viagem(rs.getInt(1));
					vv.setHora_saida(rs.getString(2));
					vv.setHora_chegada(rs.getString(3));
					vv.setNr_passageiros(rs.getInt(4));
					vv.setData_viagem(rs.getString(5));
					Comboio c = new Comboio();
					c.setId_comboio(rs.getInt(6));
					c.setNumero(rs.getShort(7));
					c.setLotacao(rs.getInt(8));
					Rota r = new Rota();
					r.setId_rota(rs.getInt(9));
					r.setNome(rs.getString(10));
					r.setEstacao_saida(rs.getString(11));
					r.setEstacao_chegada(rs.getString(12));
					c.setRota(r);
					vv.setComboio(c);
					this.v.addElement(vv);
				}
				if(v.size()==0) {
					JOptionPane.showMessageDialog(null, "Nao foram encontrados resultados para a pesquisa");
					dados=new String[v.size()][titulos.length];
					table=new JTable(dados,titulos);
					scrollPane.setViewportView(table);
				}
				else
					preencherTabela();
		}catch(SQLException | ArrayIndexOutOfBoundsException sqlE) {
			JOptionPane.showMessageDialog(null, "Erro ao filtrar dados: "+sqlE.getMessage());
			sqlE.printStackTrace();
		}
	}
	private void preencherTabela(){
		
		dados=new String [v.size()][titulos.length];
		for(int x=0;x<dados.length;x++) {
			Viagem vv=v.elementAt(x);
			dados[x][0]=vv.getData_viagem();
			dados[x][1]=vv.getComboio().getNumero()+"";
			dados[x][2]=vv.getComboio().getRota().getNome();
			dados[x][3]=vv.getComboio().getRota().getEstacao_saida();
			dados[x][4]=vv.getHora_saida();
			dados[x][5]=vv.getComboio().getRota().getEstacao_chegada();
			dados[x][6]=vv.getHora_chegada();
			dados[x][7]=vv.getNr_passageiros()+"";
		}
	}
	private void editar() {
		vv= v.get(table.getSelectedRow());
//			JOptionPane.showMessageDialog(null, opr.toString());
		
	}
}
