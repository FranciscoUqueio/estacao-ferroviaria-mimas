package janelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import conexao.BaseDados;
import modelos.Estacao;
import modelos.Validacoes;

public class ListaEstacao extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private ResultSet rs;
	PreparedStatement ps;
	private JTable table;
	private Vector<Estacao> v;
	private String [][] dados;
	private String [] titulos = {"Nome","Localizacao"};
	private Estacao estacao;
	private JScrollPane scrollPane;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListaEstacao frame = new ListaEstacao();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListaEstacao() {
		super("Lista de Esta��es");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 675, 513);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitulo = new JLabel(super.getTitle());
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(0, 0, 653, 60);
		contentPane.add(lblTitulo);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "Filtro", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 77, 628, 133);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(15, 30, 53, 20);
		panel.add(lblNome);
		
		textField = new JTextField();
		textField.setBounds(83, 27, 217, 26);
		panel.add(textField);
		textField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				Validacoes validacoes = new Validacoes();
				validacoes.validateName(textField.getText());
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		textField.setColumns(10);
		
		JLabel lblLocalizao = new JLabel("Localiza\u00E7\u00E3o");
		lblLocalizao.setBounds(310, 30, 79, 20);
		panel.add(lblLocalizao);
		
		textField_1 = new JTextField();
		textField_1.setBounds(404, 27, 209, 26);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnFiltrar = new JButton("Filtrar");
		btnFiltrar.setBounds(15, 88, 115, 29);
		//panel.add(btnFiltrar);
		
		JButton btnProcurar = new JButton("Procurar");
		btnProcurar.setBounds(498, 88, 115, 29);
		btnProcurar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				search();
			}
		});
		panel.add(btnProcurar);
		
		JPanel panResultado = new JPanel();
		panResultado.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "Resultado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panResultado.setBounds(15, 248, 623, 148);
		contentPane.add(panResultado);
		panResultado.setLayout(new GridLayout(1, 0, 0, 0));
		
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panResultado.add(scrollPane);
		
		v = new Vector<Estacao>();
		pullDataFromDB();
		fillTable();
		
		table = new JTable();
		table.setModel(new DefaultTableModel(dados,titulos));
		scrollPane.setViewportView(table);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(523, 412, 115, 29);
		btnSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		contentPane.add(btnSair);
	}
	
	public void pullDataFromDB() {
		String sql="SELECT * FROM Estacao";
		try {
			ps = BaseDados.conectar().prepareStatement(sql);
			rs = ps.executeQuery();
			createObjeEstacao(rs);
		}catch(SQLException s) {JOptionPane.showMessageDialog(null,s);}
	}
	
	public void createObjeEstacao(ResultSet resultSet) {
		try{
			while(resultSet.next()) {
				estacao = new Estacao();
				estacao.setId_estacao(resultSet.getInt(1));
				estacao.setNome(resultSet.getString(2));
				estacao.setLocalizacao(resultSet.getString(3));
				v.addElement(estacao);
			}
		}catch(SQLException s){JOptionPane.showMessageDialog(null,s);}
	}
	
	public void fillTable() {
	    dados = new String[v.size()][titulos.length];
		System.out.println(v.size());
	    for(int i=0;i<dados.length;i++) {
		    estacao = v.elementAt(i);
	    	dados[i][0]= estacao.getNome();
	    	dados[i][1]= estacao.getLocalizacao();
 	    }
	}
	
	public void search() {
		Validacoes validacoes = new Validacoes();
		validacoes.validateName(textField.getText());
		String nome = textField.getText();
		String sql = "SELECT id_estacao,nome,localizacao FROM Estacao WHERE nome LIKE '"+nome+"'";
		v.removeAllElements();
		try {
			ps = BaseDados.conectar().prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				estacao = new Estacao();
				estacao.setId_estacao(rs.getInt(1));
				estacao.setNome(rs.getString(2));
				estacao.setLocalizacao(rs.getString(3));
				v.addElement(estacao);
			}
			showSearched();
		}catch(SQLException s) {JOptionPane.showMessageDialog(null,s);}
	}
	public void showSearched() {
		dados = new String[v.size()][titulos.length];
		dados[0][0] = estacao.getNome();
		dados[0][1] = estacao.getLocalizacao();
		table = new JTable(dados,titulos);
		scrollPane.setViewportView(table);
 	}
}