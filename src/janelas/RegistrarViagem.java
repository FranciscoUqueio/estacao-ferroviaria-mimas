package janelas;

import java.awt.EventQueue;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.basic.BasicScrollPaneUI.HSBChangeListener;
import javax.swing.text.MaskFormatter;

import conexao.BaseDados;
import modelos.Comboio;
import modelos.Comboio_Maquinista;
import modelos.Rota;

import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.border.EtchedBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


@SuppressWarnings("javadoc")
public class RegistrarViagem extends JFrame {
	private JPanel contentPane;
	private JLabel lblTitulo;
	private JPanel panDadosViagem;
	private JLabel lblRotaComboio;
	private JComboBox cbxComboio;
	private JLabel lblRota;
	private JTextField txtEstacaoPartida;
	private JLabel lblData;
	private JFormattedTextField txtData;
	private JLabel lblHoraDePartida;
	private JFormattedTextField txtHoraPartida;
	private JFormattedTextField txtHoraChegada;
	private JLabel lblHoraChegada;
	private JTextField txtEstacaoDeChegada;
	private JLabel lblPartida;
	private JLabel lblChegada;
	private JLabel lblPassageiros;
	private JFormattedTextField txtPassageiros;
	private JButton btnSalvar;
	private JButton btnActualizar;
	private JButton btnSair;
	private PreparedStatement ps;
	private ResultSet rs;
	private Vector<Comboio> v=new Vector<Comboio>();
//	private Connection conn=BaseDados.conectar();
	protected int cbxind;
	protected Date dateTime2;
	protected Date dateTime1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					RegistrarViagem frame = new RegistrarViagem();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public RegistrarViagem() throws Exception {
		super("Registrar Viagem");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 742, 397);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblTitulo = new JLabel(super.getTitle());
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(0, 0, 720, 60);
		contentPane.add(lblTitulo);
		
		panDadosViagem = new JPanel();
		panDadosViagem.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Dados da Viagem", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panDadosViagem.setBounds(10, 76, 698, 207);
		contentPane.add(panDadosViagem);
		panDadosViagem.setLayout(null);
		
		lblRotaComboio = new JLabel("Rota");
		lblRotaComboio.setHorizontalAlignment(SwingConstants.CENTER);
		lblRotaComboio.setBounds(15, 45, 116, 26);
		panDadosViagem.add(lblRotaComboio);
		
		preencherComboio();
		String s[]=new String[v.size()+1];
		int x=1;
		s[0]=" ";
//		System.out.println(v.size());
		for(Comboio c: v) {
			s[x]=c.getNumero()+" - "+c.getRota().getNome();
			x++;
		}
		cbxComboio = new JComboBox(s);
//		cbxComboio.setSelectedIndex(0);
		cbxComboio.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				cbxind=cbxComboio.getSelectedIndex();
				if(cbxind==0) {
					btnSalvar.setEnabled(false);
					txtEstacaoPartida.setText("Estacao de Partida");
					txtEstacaoDeChegada.setText("Estacao de Chegada");
				}
				else {
					txtEstacaoPartida.setText(v.elementAt(cbxind).getRota().getEstacao_saida());
					txtEstacaoDeChegada.setText(v.elementAt(cbxind).getRota().getEstacao_chegada());					
				}
			}
		});
		cbxComboio.setBounds(146, 42, 156, 26);
		panDadosViagem.add(cbxComboio);
		
		lblRota = new JLabel("Rota");
		lblRota.setHorizontalAlignment(SwingConstants.CENTER);
		lblRota.setBounds(333, 16, 350, 20);
		panDadosViagem.add(lblRota);
		
		txtEstacaoPartida = new JTextField();
		txtEstacaoPartida.setText("Estacao de Partida");
		txtEstacaoPartida.setHorizontalAlignment(SwingConstants.CENTER);
		txtEstacaoPartida.setEditable(false);
		txtEstacaoPartida.setBounds(479, 45, 204, 26);
		panDadosViagem.add(txtEstacaoPartida);
		
		lblData = new JLabel("Data");
		lblData.setHorizontalAlignment(SwingConstants.CENTER);
		lblData.setBounds(15, 88, 116, 20);
		panDadosViagem.add(lblData);
		
		DateFormat data=new SimpleDateFormat("dd/MM/yyyy");
		txtData = new JFormattedTextField(new MaskFormatter("##/##/####"));
		txtData.setHorizontalAlignment(SwingConstants.CENTER);
		txtData.setEditable(false);
		txtData.setText(data.format(System.currentTimeMillis()));
		txtData.setBounds(146, 85, 156, 26);
		panDadosViagem.add(txtData);
		
		lblHoraDePartida = new JLabel("Hora de Partida");
		lblHoraDePartida.setHorizontalAlignment(SwingConstants.CENTER);
		lblHoraDePartida.setBounds(15, 124, 116, 20);
		panDadosViagem.add(lblHoraDePartida);
		
		MaskFormatter horaFormato =new MaskFormatter("##:##");
		txtHoraPartida = new JFormattedTextField(horaFormato);
		txtHoraPartida.setHorizontalAlignment(SwingConstants.CENTER);
		DateFormat formatter = new SimpleDateFormat("HH:mm");
		txtHoraPartida.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String s=txtHoraPartida.getText();
				try {
				dateTime2= formatter.parse(s);
//				System.out.println("Horas: "+s);
					byte hora=Byte.parseByte(s.substring(0,2));
//					System.out.println("hora: "+hora);
					if(hora<0 || hora>23 ) {
						JOptionPane.showMessageDialog(null, "Hora invalida");
						txtHoraPartida.setText("");
					}
					byte minutos=Byte.parseByte(s.substring(3,5));
//					System.out.println("Minutos: "+minutos);
					if(hora==23 && minutos>44) {
						JOptionPane.showMessageDialog(null, "Minutos inv�lidos\n Os Comboios nao estao em funcionamento ap�s as 23:44");
						txtHoraPartida.setText(hora+"44");
					}else
						if(hora<23 && minutos>59) {
							JOptionPane.showMessageDialog(null, "Minutos inv�lidos\n ");
							txtHoraPartida.setText(hora+"");
					}
					
				}catch(NumberFormatException | ParseException nfe) {}
			}
		});
		txtHoraPartida.setBounds(146, 121, 80, 23);
		
		panDadosViagem.add(txtHoraPartida);
		
		txtHoraChegada = new JFormattedTextField(horaFormato);
		txtHoraChegada.setHorizontalAlignment(SwingConstants.CENTER);
		txtHoraChegada.setBounds(146, 155, 80, 26);
		txtHoraChegada.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String hChegada=txtHoraChegada.getText(),hPartida=txtHoraPartida.getText();
//				System.out.println("Horas: "+s);
				

				try {
				dateTime1= formatter.parse(hChegada);
				dateTime2=formatter.parse(hPartida);
				long diffInMinutes = dateTime1.getTime()-dateTime2.getTime();			
				byte days = (byte) ((diffInMinutes / (1000*60)));
					byte hora=Byte.parseByte(hChegada.substring(0,2));
//					System.out.println("hora: "+hora);
					if(hora<0 || hora>23 ) {
						JOptionPane.showMessageDialog(null, "Hora invalida");
						txtHoraChegada.setText("");
					}
					byte minutos=Byte.parseByte(hChegada.substring(3,5));
//					System.out.println("Minutos: "+minutos);
					if(minutos<0 || minutos>59) {
						JOptionPane.showMessageDialog(null, "Minutos inv�lidos");
						txtHoraChegada.setText(hora+"");
					}
					if(days<15) {
						JOptionPane.showMessageDialog(null, "Duracao Invalida\nAs rotas tem duracao minima de 15 minutos");
						txtHoraChegada.setText("");
//						txtHoraPartida.requestFocusInWindow();
						txtHoraPartida.setText("");
					}
					else
						txtHoraChegada.nextFocus();
				}catch(NumberFormatException | ParseException nfe) {}
			}
		});
		panDadosViagem.add(txtHoraChegada);
		
		lblHoraChegada = new JLabel("Hora de Chegada");
		lblHoraChegada.setHorizontalAlignment(SwingConstants.CENTER);
		lblHoraChegada.setBounds(15, 158, 116, 20);
		panDadosViagem.add(lblHoraChegada);
		
		txtEstacaoDeChegada = new JTextField();
		txtEstacaoDeChegada.setText("Estacao de Chegada");
		txtEstacaoDeChegada.setHorizontalAlignment(SwingConstants.CENTER);
		txtEstacaoDeChegada.setEditable(false);
		txtEstacaoDeChegada.setBounds(479, 84, 204, 26);
		panDadosViagem.add(txtEstacaoDeChegada);
		
		lblPartida = new JLabel("Partida");
		lblPartida.setHorizontalAlignment(SwingConstants.CENTER);
		lblPartida.setBounds(333, 45, 131, 20);
		panDadosViagem.add(lblPartida);
		
		lblChegada = new JLabel("Chegada");
		lblChegada.setHorizontalAlignment(SwingConstants.CENTER);
		lblChegada.setBounds(333, 88, 131, 20);
		panDadosViagem.add(lblChegada);
		
		lblPassageiros = new JLabel("Passageiros");
		lblPassageiros.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassageiros.setBounds(333, 124, 131, 20);
		panDadosViagem.add(lblPassageiros);
		
		txtPassageiros = new JFormattedTextField(new MaskFormatter("###"));
		txtPassageiros.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				int limite=v.elementAt(cbxind).getLotacao();
				try {
					if(Integer.parseInt(txtPassageiros.getText())>limite) {
						JOptionPane.showMessageDialog(null, "Excedeu o limite");
						txtPassageiros.setText(limite+"");
						txtPassageiros.selectAll();
					}
					else
						txtPassageiros.nextFocus();
					
				}catch(NumberFormatException nfe) {}
			}
		});
		txtPassageiros.setBounds(479, 123, 80, 23);
//		txtPassageiros.addFocusListener(new FocusAdapter() {
//			
//			@Override
//			public void focusLost(FocusEvent e) {
//				// TODO Auto-generated method stub
//								
//			}
//			
//		});
		panDadosViagem.add(txtPassageiros);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				adicionar();
			}
		});
		btnSalvar.setBounds(164, 299, 115, 29);
		contentPane.add(btnSalvar);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					new ListaViagem().setTitle("Actualizacao de dados");
					new ListaViagem().setVisible(true);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnActualizar.setBounds(325, 299, 115, 29);
		contentPane.add(btnActualizar);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSair.setBounds(494, 299, 115, 29);
		contentPane.add(btnSair);
	}

	private void preencherComboio() {
		// TODO Auto-generated method stub
		v.removeAllElements();
		Comboio c;
		Rota r;
		try {
			String sql="select c.*, r.nome,r.estacao_saida,r.estacao_chegada \nfrom rota r join comboio c on r.id_rota=c.id_rota";
//			sql="select * from viage";
			System.out.println(sql);
			ps=BaseDados.conectar().prepareStatement(sql);
			rs=ps.executeQuery();
//			System.out.println(rs.next());
			while(rs.next()) {
				c=new Comboio();
				c.setId_comboio(rs.getInt(1));
				c.setNumero(rs.getShort(2));
				c.setLotacao(rs.getInt(3));
				c.setNr_vagoes(rs.getInt(4));
				c.setEm_movimento(rs.getInt(5));
				r=new Rota();
				r.setId_rota(rs.getInt(6));
				r.setNome(rs.getString(7));
				r.setEstacao_saida(rs.getString(8));
				r.setEstacao_chegada(rs.getString(9));
				c.setRota(r);
				v.addElement(c);
			}
//			JOptionPane.showMessageDialog(null, "preenchido com sucesso");
			
		} catch (SQLException e) {JOptionPane.showMessageDialog(null, "Erro ao adicionar Viagem: "+e.getMessage());
		}
		
	}
	private void adicionar() {
		
		try {
			int comboio=v.elementAt(cbxind-1).getId_comboio(),nrPassageiros=Integer.parseInt(txtPassageiros.getText());
			String data=txtData.getText(),horaP=txtHoraPartida.getText(),horaC=txtHoraChegada.getText();
//			String a="select max(id_oper_revisao) from operador_de_revisao";
//			ps=BaseDados.conectar().prepareStatement(a);
//			rs=ps.executeQuery();
//			while(rs.next())
//				JOptionPane.showMessageDialog(null, rs.getObject(1));
			String plscript = "DECLARE \n\tl_id viagem.id_viagem%TYPE; \nBEGIN\n\tSELECT MAX(id_viagem)+1\n INTO l_id\n"
					+ "FROM viagem;\n";
			String sql="INSERT INTO viagem (id_viagem,hora_saida,hora_chegada,nr_passageiros,data_viagem,id_comboio)"
					+ "\nVALUES(l_id,?,?,?,?,?);\n";
			plscript+=sql+"END;";
			System.out.println(plscript);
			ps=BaseDados.conectar().prepareStatement(plscript);
			ps.setString(1, horaP);
			ps.setString(2, horaC);
			ps.setInt(3, nrPassageiros);
			ps.setString(4, data);
			ps.setInt(5, comboio);
			ps.execute();
			JOptionPane.showMessageDialog(null, "Viagem \""+v.elementAt(cbxind-1).getRota().getNome()+"\" adicionada com sucesso");
			
			
		}catch(SQLException sqlE) {JOptionPane.showMessageDialog(null, "Erro ao adicionar Viagem: "+sqlE.getMessage());}
	}
}
